# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2018-10-31
### Added
- Initial version

## [1.1.0] - 2022-10-29
### Added
- Command line argument to change the sleep time between reading cycles

## [1.2.0] - 2022-11-24
### Added
- Command line argument to log serial communication to file

## [1.3.0] - 2023-01-10
### Changed
- Changed data capture functionality for more robustness. New state machine.
