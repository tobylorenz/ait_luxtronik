# Introduction

This repository contains multiple programs for Luxtronik 1 from alpha innotec.

luxtronik2mqtt is an MQTT clien that provides a gateway between the serial port
of the Luxtronik 1 and MQTT on the other hand.

collectd-luxtronik makes use of collectd to capture the temperatures.
I recommend drraw to draw the graphs.

luxtronik-heizkurve captures the Aussen-Ist and Rücklauf-Soll temperatures and
draw the heating curve.

luxtronik-monitor monitors changes to the Luxtronik configuration. So this is
to observe the error log and to check what the service technician changed.

luxtronik-plan is a cgi-bin that shows a plan of the heating system including
real-time values.

luxtronik-simulator simulates a Luxtronik on a serial port. This is mainly
for test purposes and to monitor what the original software does.

luxtronik-terminal simulates the terminal of the original software.

# Build on Linux (e.g. Debian Testing)

Building under Linux works as usual:

    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr ..
    cmake --build . --target all
    ctest
    cpack

# Tests

Static tests are

* Cppcheck (if OPTION_RUN_CPPCHECK is set)
* CCCC (if OPTION_RUN_CCCC is set)

Dynamic tests are

* Unit tests (if OPTION_BUILD_TESTS is set)
* Example runs (if OPTION_BUILD_EXAMPLES is set)
* Coverage (if OPTION_ADD_LCOV is set)

# Source of Luxtronik information

* Our Roth TerraCompact 23kW, which is inside an alpha innotec SWC 230.
* [http://www.heatpump24.de/kontoeinst_104.php?layout=1](Luxtronik Software)
* [http://files.ait-group.net/alp/?lang=de](Bedienungsanleitungen)
** Bedienungsanleitung SWC 230
** [http://files.ait-group.net/upload/Alpha-InnoTec/Betriebsanleitungen/W%E4rmepumpen/06%20Zubeh%F6r/Regler/Regler_alt/Luxtronik%20I/Luxtronik%20I_alt/830098_160130_BA_Modemanbindung.pdf](Modemanbindung.pdf)
* [http://www.alpha-innotec.ch/alpha-innotec/produkte/internet-anbindung.html](alpha web | alpha app) Demo login
* [www.haustechnikdialog.de](HaustechnikDialog)
* [knx-user-formum.de](KNX-User-Forum)

# Standards

* MQTT topics roughly follow the Homie Convention https://github.com/homieiot/convention in terms
  of qos=1, retain=yes, $name, and $unit, $state.
