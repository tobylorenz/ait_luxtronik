/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

/* C includes */
#include <unistd.h>

/* C++ includes */
#include <atomic>
#include <chrono>
#include <csignal>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <mosquittopp.h>
#include <thread>

/* project internal includes */
#include "MqttClient.h"

/** clear screen */
void clearScreen()
{
    std::cout << "\x1b[2J\x1b[1;1H";
}

std::string tryGetTopic(const MqttClient * mqttClient, std::string topic)
{
    try {
        return mqttClient->getTopic(topic);
    } catch (const std::exception &) {
        return "";
    }
}

/** 1: Anlagenstatus */
void anlagenstatus(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Anlagenstatus" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "WP-Typ          " << tryGetTopic(mqttClient, "physical/1700/2") << std::endl;
    std::cout << "SW-Stand        " << tryGetTopic(mqttClient, "physical/1700/3") << std::endl;
    std::cout << "Biv.-Stufe      " << tryGetTopic(mqttClient, "physical/1700/4") << std::endl;
    std::cout << "Betr.-Zust.     " << tryGetTopic(mqttClient, "physical/1700/5") << std::endl;
    std::cout << "Anlagenkonf.:   " << tryGetTopic(mqttClient, "physical/2800/2") << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "CAN-BUS enabled" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "EIN seit:       " << tryGetTopic(mqttClient, "physical/1700/6") << " "
                                    << tryGetTopic(mqttClient, "physical/1700/7") << std::endl;

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 2: Temperaturen */
void temperaturen(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Temperaturen" << std::endl;
    std::cout << std::endl;
    std::cout << "Vorlauf        " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/2") << " C" << std::endl;
    std::cout << "MK1-Vorl.      " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/11") << " C" << std::endl;
    std::cout << "MK1VL-Soll     " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/12") << " C" << std::endl;
    std::cout << "Rücklauf       " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/3") << " C" << std::endl;
    std::cout << "RL-Soll        " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/4") << " C" << std::endl;
    std::cout << "Heissgas       " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/5") << " C" << std::endl;
    std::cout << "Aussent.       " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/6") << " C" << std::endl;
    std::cout << "BW-Ist         " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/7") << " C" << std::endl;
    std::cout << "BW-Soll        " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/8") << " C" << std::endl;
    std::cout << "WQ-Ein         " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/9") << " C" << std::endl;
    std::cout << "Kältekreis     " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/10") << " C" << std::endl;
    std::cout << "Raumstat.      " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/1100/13") << " C" << std::endl;

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 3: Eingaenge + Ausgaenge */
void eingaengeAusgaenge(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Eingaenge     + Ausgaenge" << std::endl;
    std::cout << std::endl;
    std::cout << " Eingaenge     -------" << std::endl;
    std::cout << std::endl;
    std::cout << "ASD               " << tryGetTopic(mqttClient, "physical/1200/2") << std::endl;
    std::cout << "BWT               ???" << std::endl; // @todo BWT
    std::cout << "EVU               " << tryGetTopic(mqttClient, "physical/1200/3") << std::endl;
    std::cout << "HD                " << tryGetTopic(mqttClient, "physical/1200/4") << std::endl;
    std::cout << "MOT               " << tryGetTopic(mqttClient, "physical/1200/5") << std::endl;
    std::cout << "ND                " << tryGetTopic(mqttClient, "physical/1200/6") << std::endl;
    std::cout << "PEX               " << tryGetTopic(mqttClient, "physical/1200/7") << std::endl;
    std::cout << std::endl;
    std::cout << " Ausgaenge     -------" << std::endl;
    std::cout << std::endl;
    std::cout << "Verdichter 1      " << tryGetTopic(mqttClient, "physical/1300/10") << std::endl;
    std::cout << "Verdichter 2      " << tryGetTopic(mqttClient, "physical/1300/11") << std::endl;
    std::cout << "Abtauventil       " << tryGetTopic(mqttClient, "physical/1300/2") << std::endl;
    std::cout << "ZWE 1             " << tryGetTopic(mqttClient, "physical/1300/13") << std::endl;
    std::cout << "ZWE 2 - SST       " << tryGetTopic(mqttClient, "physical/1300/14") << std::endl;
    std::cout << "Mischer 1 Auf     " << tryGetTopic(mqttClient, "physical/1300/6") << std::endl;
    std::cout << "Mischer 1 Zu      " << tryGetTopic(mqttClient, "physical/1300/7") << std::endl;
    std::cout << "Ventil.- BOSUP    " << tryGetTopic(mqttClient, "physical/1300/9") << std::endl;
    std::cout << "Ventilation       " << tryGetTopic(mqttClient, "physical/1300/8") << std::endl;
    std::cout << "HUP               " << tryGetTopic(mqttClient, "physical/1300/5") << std::endl;
    std::cout << "FUP 1             " << tryGetTopic(mqttClient, "physical/1300/4") << std::endl;
    std::cout << "BUP               " << tryGetTopic(mqttClient, "physical/1300/3") << std::endl;
    std::cout << "ZUP - ZIP         " << tryGetTopic(mqttClient, "physical/1300/12") << std::endl;
    std::cout << "LED               ???" << std::endl; // @todo LED

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 4: Einstellungen : Temperaturen */
void einstellungenTemperaturen(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "KD-Einstellungen Temperaturen" << std::endl;
    std::cout << std::endl;
    std::cout << "Warmer/Kalter   ????? ?" << std::endl; // @todo Warmer/Kalter
    std::cout << "TR Begrenz      " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/2") << " C" << std::endl;
    std::cout << "Hysterese HR    " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/3") << " K" << std::endl;
    std::cout << "TR Erh max      " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/4") << " K" << std::endl;
    std::cout << "TFreig 2.VD     " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/5") << " C" << std::endl;
    std::cout << "Freig. ZWE      " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/6") << " C" << std::endl;
    std::cout << "T-Luftabt.      " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/7") << " C" << std::endl;
    std::cout << "T-LGS soll      " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/8") << " C" << std::endl;
    std::cout << "Hysterese BW    " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/9") << " C" << std::endl;
    std::cout << "TV 2VD BW       " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/10") << " C" << std::endl;
    std::cout << "TAussen max     " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/11") << " C" << std::endl;
    std::cout << "TAussen min     " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/12") << " C" << std::endl;
    std::cout << "T-WQ min        " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/13") << " C" << std::endl;
    std::cout << "T-HG max        " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/14") << " C" << std::endl;
    std::cout << "T-LAbtEnde      " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/15") << " C" << std::endl;
    std::cout << "Absenk. bis     " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/16") << " C" << std::endl;
    std::cout << "Vorlauf max.    " << std::right << std::setfill(' ') << std::setw(5) << tryGetTopic(mqttClient, "physical/2100/17") << " C" << std::endl;

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 5: Einstellungen : System Einstellung */
void einstellungenSystemEinstellung(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "KD-Einstellungen System" << std::endl;
    std::cout << std::endl;
    std::cout << "EVU              " << tryGetTopic(mqttClient, "physical/2200/2") << std::endl;
    std::cout << "Raumstation      " << tryGetTopic(mqttClient, "physical/2200/3") << std::endl;
    std::cout << "Einbindung       " << tryGetTopic(mqttClient, "physical/2200/4") << std::endl;
    std::cout << "Mischkreis1      " << tryGetTopic(mqttClient, "physical/2200/5") << std::endl;
    std::cout << "ZWE1 Art         " << tryGetTopic(mqttClient, "physical/2200/6") << std::endl;
    std::cout << "ZWE1 Fkt         " << tryGetTopic(mqttClient, "physical/2200/7") << std::endl;
    std::cout << "ZWE2 Art         " << tryGetTopic(mqttClient, "physical/2200/8") << std::endl;
    std::cout << "ZWE2 Fkt         " << tryGetTopic(mqttClient, "physical/2200/9") << std::endl;
    std::cout << "Stoerung         " << tryGetTopic(mqttClient, "physical/2200/10") << std::endl;
    std::cout << "Brauchwasser     " << tryGetTopic(mqttClient, "physical/2200/11") << std::endl;
    std::cout << "Brauchw. mit     " << tryGetTopic(mqttClient, "physical/2200/12") << std::endl;
    std::cout << "BW-Bereit.       " << tryGetTopic(mqttClient, "physical/2200/13") << std::endl;
    std::cout << "Brauchw.4        " << tryGetTopic(mqttClient, "physical/2200/14") << std::endl;
    std::cout << "Brauchw.5        ?????" << std::endl; // @todo Brauchw.5 "mit HUP"
    std::cout << "BW&WP max        " << tryGetTopic(mqttClient, "physical/2200/15") << " " << tryGetTopic(mqttClient, "physical/2200/15/$unit") << std::endl;
    std::cout << "Abtzyk max       " << tryGetTopic(mqttClient, "physical/2200/16") << " " << tryGetTopic(mqttClient, "physical/2200/16/$unit") << std::endl;
    std::cout << "Luftabt.         " << tryGetTopic(mqttClient, "physical/2200/17") << std::endl;
    std::cout << "Labtzeit max     " << tryGetTopic(mqttClient, "physical/2200/18") << " " << tryGetTopic(mqttClient, "physical/2200/18/$unit") << std::endl;
    std::cout << "ABT_Manager      ?????" << std::endl; // @todo ABT_Manager "------"
    std::cout << "Abtauen 1        " << tryGetTopic(mqttClient, "physical/2200/19") << std::endl;
    std::cout << "Pumpenopt.       " << tryGetTopic(mqttClient, "physical/2200/21") << std::endl;
    std::cout << "Zusatzpumpe      " << tryGetTopic(mqttClient, "physical/2200/22") << std::endl;
    std::cout << "Datenzugang      " << tryGetTopic(mqttClient, "physical/2200/23") << std::endl;
    std::cout << "ASD              " << tryGetTopic(mqttClient, "physical/2200/24") << std::endl;
    std::cout << "Ueberw. VD       " << tryGetTopic(mqttClient, "physical/2200/25") << std::endl;
    std::cout << "Regelung         " << tryGetTopic(mqttClient, "physical/2200/26") << std::endl;
    std::cout << "Ausheizen        " << tryGetTopic(mqttClient, "physical/2200/27") << std::endl;
    std::cout << "Par.-Betr.       " << tryGetTopic(mqttClient, "physical/2200/28") << std::endl;
    std::cout << "Periode 1        " << tryGetTopic(mqttClient, "physical/2200/30") << std::endl;
    std::cout << "Laufzeit 1       " << tryGetTopic(mqttClient, "physical/2200/31") << std::endl;

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 6: Ablaufzeiten (1) */
void ablaufzeiten1(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Ablaufzeiten (1)" << std::endl;
    std::cout << std::endl;
    std::cout << "BStd VD1          " << std::right << std::setfill('0') << std::setw(12) << tryGetTopic(mqttClient, "physical/1450/2") << std::endl;
    std::cout << "Imp. VD1          " << std::right << std::setfill('0') << std::setw(6) << tryGetTopic(mqttClient, "physical/1450/3") << std::endl;
    std::cout << "d.EZ VD2          " << std::right << std::setfill('0') << std::setw(12) << tryGetTopic(mqttClient, "physical/1450/4") << std::endl;
    std::cout << "BStd VD2          " << std::right << std::setfill('0') << std::setw(12) << tryGetTopic(mqttClient, "physical/1450/5") << std::endl;
    std::cout << "Imp. VD2          " << std::right << std::setfill('0') << std::setw(6) << tryGetTopic(mqttClient, "physical/1450/6") << std::endl;
    std::cout << "d.EZ VD2          " << std::right << std::setfill('0') << std::setw(12) << tryGetTopic(mqttClient, "physical/1450/7") << std::endl;
    std::cout << "BSt ZWE1          " << std::right << std::setfill('0') << std::setw(12) << tryGetTopic(mqttClient, "physical/1450/8") << std::endl;
    std::cout << "BSt ZWE2          " << std::right << std::setfill('0') << std::setw(12) << tryGetTopic(mqttClient, "physical/1450/9") << std::endl;
    std::cout << std::endl;
    std::cout << "BStd WP           " << std::right << std::setfill('0') << std::setw(12) << tryGetTopic(mqttClient, "physical/1450/10") << std::endl;
    std::cout << std::endl;
    std::cout << "WP ein seit       " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/2-4") << " h" << std::endl;
    std::cout << "ZWE1 seit         " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/5-7") << " h" << std::endl;
    std::cout << "ZWE2 seit         " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/8-10") << " h" << std::endl;
    std::cout << "Netzeinverz       " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/11") << " s" << std::endl;
    std::cout << "SSP-Einschalt:    " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/14-15") << " min" << std::endl;
    std::cout << "VD-Standz         " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/16-18") << " h" << std::endl;
    std::cout << "aktuell VD1:      ??:??:?? h" << std::endl; // @todo aktuell VD1
    std::cout << "aktuell VD2:      ??:??:?? h" << std::endl; // @todo aktuell VD2
    std::cout << "HRM-Zeit          " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/19-21") << " h" << std::endl;
    std::cout << "HRW-Zeit          " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/22-24") << " h" << std::endl;
    std::cout << "BIV-W-Zeit:       ??:??:?? h" << std::endl; // @todo BIV-W-Zeit
    std::cout << "LGS seit          ??:??:?? h" << std::endl; // @todo LGS seit
    std::cout << "Sperre BW   ----  " << std::right << std::setfill('0') << std::setw(8) << tryGetTopic(mqttClient, "physical/1400/28-30") << " h" << std::endl;
    std::cout << "UEG seit          ??:??:?? h" << std::endl; // @todo UEG seit
    std::cout << "OEG seit          ??:??:?? h" << std::endl; // @todo OEG seit

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 7: Ablaufzeiten (2) */
void ablaufzeiten2(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Ablaufzeiten (2)" << std::endl;
    std::cout << std::endl;
    std::cout << "PUMPENOPTIMIERUNG:     gesperrt" << std::endl; // @todo Pumpenoptimierung
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "HEIZUNGSPUMPE:           laeuft" << std::endl; // @todo Heizungspumpe
    std::cout << "HUP - Vorlauf:         0:00 min" << std::endl; // @todo HUP - Vorlauf
    std::cout << "HUP - Nachlauf 2:      0:00 min" << std::endl; // @todo HUP - Nachlauf 2
    std::cout << "HUP - Nachlauf 4:      0:00 min" << std::endl; // @todo HUP - Nachlauf 4
    std::cout << "HUP - Standzeit:  150:00:00 h  " << std::endl; // @todo HUP - Standzeit
    std::cout << std::endl;
    std::cout << "ZUSATZUMWAELZPUMPE: nicht def. " << std::endl; // @todo ZUP
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "ZIRKULATIONSPUMPE:     gesperrt" << std::endl; // @todo ZIP
    std::cout << "ZIP-Standzeit:         0:00 min" << std::endl; // @todo ZIP-Standzeit
    std::cout << std::endl;
    std::cout << "VENTILATION:              steht" << std::endl; // @todo Ventilation
    std::cout << "VENT - Standzeit:     0:00:00 h" << std::endl; // @todo Ventilation-Standzeit
    std::cout << std::endl;
    std::cout << "BRUNNEN-/SOLEPUMPE:       steht" << std::endl; // @todo BOSUP
    std::cout << std::endl;
    std::cout << "ENTLUEFTUNGSPROGRAMM:      nein" << std::endl; // @todo Entlüftungsprogramm
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "VD-Red: 0   BIV-Red: 0    TtT: 000 s" << std::endl; // @todo VD-Red, BIV-Red, TtT

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 8: Heizung + Brauchwasser */
void heizungBrauchwasser(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Heizung + Brauchwasser" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    // @todo Heizung + Brauchwasser

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 9: gesp. Fehler + Abschaltungen */
void gespFehlerAbschaltungen(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Gespeicherte Fehler und Abschaltungen" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << " FEHLERSPEICHER" << std::endl;
    std::cout << std::endl;
    std::cout << "E " << tryGetTopic(mqttClient, "raw/1500/1500/3") << ": "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1500/1500/3") << " "
              << tryGetTopic(mqttClient, "physical/1500/1500/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1500/1500/7-8") << std::endl;
    std::cout << "E " << tryGetTopic(mqttClient, "raw/1500/1501/3") << ": "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1500/1501/3") << " "
              << tryGetTopic(mqttClient, "physical/1500/1501/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1500/1501/7-8") << std::endl;
    std::cout << "E " << tryGetTopic(mqttClient, "raw/1500/1502/3") << ": "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1500/1502/3") << " "
              << tryGetTopic(mqttClient, "physical/1500/1502/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1500/1502/7-8") << std::endl;
    std::cout << "E " << tryGetTopic(mqttClient, "raw/1500/1503/3") << ": "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1500/1503/3") << " "
              << tryGetTopic(mqttClient, "physical/1500/1503/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1500/1503/7-8") << std::endl;
    std::cout << "E " << tryGetTopic(mqttClient, "raw/1500/1504/3") << ": "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1500/1504/3") << " "
              << tryGetTopic(mqttClient, "physical/1500/1504/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1500/1504/7-8") << std::endl;
    std::cout << std::endl;
    std::cout << " ABSCHALTUNGEN" << std::endl;
    std::cout << std::endl;
    std::cout << "AB  1:   "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1600/1600/3") << " "
              << tryGetTopic(mqttClient, "physical/1600/1600/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1600/1600/7-8") << std::endl;
    std::cout << "AB  2:   "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1600/1601/3") << " "
              << tryGetTopic(mqttClient, "physical/1600/1601/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1600/1601/7-8") << std::endl;
    std::cout << "AB  3:   "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1600/1602/3") << " "
              << tryGetTopic(mqttClient, "physical/1600/1602/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1600/1602/7-8") << std::endl;
    std::cout << "AB  4:   "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1600/1603/3") << " "
              << tryGetTopic(mqttClient, "physical/1600/1603/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1600/1603/7-8") << std::endl;
    std::cout << "AB  5:   "
              << std::left << std::setfill(' ') << std::setw(7) << tryGetTopic(mqttClient, "physical/1600/1604/3") << " "
              << tryGetTopic(mqttClient, "physical/1600/1604/4-6") << " "
              << tryGetTopic(mqttClient, "physical/1600/1604/7-8") << std::endl;

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 10: Diagnose-Datenspeicher */
void diagnoseDatenspeicher(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << "DIAGNOSESPEICHER" << std::endl;
    // @todo 4100

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 11: Diagnose Ausheizen */
void diagnoseAusheizen(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "AUSHEIZPROGRAMM UND DIAGNOSEDATENSPEICHER" << std::endl;
    // @todo Diagnose Ausheizen

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 12: Datenzugang */
void datenzugang(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Bitte geben Sie Ihr Passwort ein:" << std::endl;
    // @todo Datenzugang

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 13: Diagnose Flaechenkuehlung */
void diagnoseFlaechenkuehlung(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Flaechenkuehlung (KKP)" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Mischkreis ohne Kuehlung konfiguriert" << std::endl; // @todo Diagnose Flaechenkuehlung

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

/** 15: Diagnose Modemeinstellungen */
void diagnoseModemeinstellungen(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "Diagnose Modemeinstellungen" << std::endl;
    std::cout << std::endl;
    std::cout << "Passwort:         ??????" << std::endl; // @todo Passwort: XXXXXX
    std::cout << "Geraete-ID:       ??????" << std::endl; // @todo Geraete-ID: 000000
    std::cout << "Abheben nach      ??????" << std::endl; // @todo Abheben nach: 4 Rufzeichen

    /* pause */
    std::string str;
    std::getline(std::cin, str);
}

bool hauptMenue(const MqttClient * mqttClient)
{
    clearScreen();
    std::cout << std::endl;
    std::cout << "  Diagnose:" << std::endl;
    std::cout << std::endl;
    std::cout << "  Version: " << tryGetTopic(mqttClient, "physical/1700/3") << "  06.12.07" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Datenzugang        ??????" << std::endl; // @todo Datenzugang
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << " 1: Anlagenstatus" << std::endl;
    std::cout << " 2: Temperaturen" << std::endl;
    std::cout << " 3: Eingaenge     + Ausgaenge" << std::endl;
    std::cout << " 4: Einstellungen     : Temperaturen" << std::endl;
    std::cout << " 5: Einstellungen     : System Einstellung" << std::endl;
    std::cout << " 6: Ablaufzeiten  (1)" << std::endl;
    std::cout << " 7: Ablaufzeiten  (2)" << std::endl;
    std::cout << " 8: Heizung + Brauchwasser" << std::endl;
    std::cout << " 9: gesp. Fehler  + Abschaltungen" << std::endl;
    std::cout << "10: Diagnose-Datenspeicher" << std::endl;
    std::cout << "11: Diagnose Ausheizen" << std::endl;
    std::cout << "12: Datenzugang:" << std::endl;
    std::cout << "13: Diagnose Flaechenkuehlung" << std::endl;
    std::cout << std::endl;
    std::cout << "15: Diagnose Modemeinstellungen" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Ziffern eingeben:" << std::endl;

    /* get and evaluate selection */
    std::string str;
    std::getline(std::cin, str);
    int selection = 0;
    try {
        selection = std::stoul(str);
    } catch (const std::exception &) {
    }
    switch(selection) {
    case 0:
        return false;
    case 1:
        anlagenstatus(mqttClient);
        break;
    case 2:
        temperaturen(mqttClient);
        break;
    case 3:
        eingaengeAusgaenge(mqttClient);
        break;
    case 4:
        einstellungenTemperaturen(mqttClient);
        break;
    case 5:
        einstellungenSystemEinstellung(mqttClient);
        break;
    case 6:
        ablaufzeiten1(mqttClient);
        break;
    case 7:
        ablaufzeiten2(mqttClient);
        break;
    case 8:
        heizungBrauchwasser(mqttClient);
        break;
    case 9:
        gespFehlerAbschaltungen(mqttClient);
        break;
    case 10:
        diagnoseDatenspeicher(mqttClient);
        break;
    case 11:
        diagnoseAusheizen(mqttClient);
        break;
    case 12:
        datenzugang(mqttClient);
        break;
    case 13:
        diagnoseFlaechenkuehlung(mqttClient);
        break;
    case 15:
        diagnoseModemeinstellungen(mqttClient);
        break;
    }
    return true;
}

/** abort the main loop */
std::atomic<bool> abortLoop;

/** handle SIGTERM */
void signalHandler(int /*signum*/)
{
    abortLoop = true;
}

/** main function */
int main(int argc, char ** argv)
{
    /* default parameters */
    std::string host = "localhost";
    int port = 1883;
    int qos = 1;
    std::string topic = "heizung/luxtronik";
    std::string id = "luxtronik-terminal";
    std::string username = "";
    std::string password = "";

    /* evaluate command line parameters */
    int c;
    while ((c = getopt (argc, argv, "h:p:q:t:i:u:P:d:c:")) != -1) {
        switch (c) {
        case 'h':
            host = optarg;
            break;
        case 'p':
            port = std::stoul(optarg);
            break;
        case 'q':
            qos = std::stoul(optarg);
            break;
        case 't':
            topic = optarg;
            break;
        case 'i':
            id = optarg;
        case 'u':
            username = optarg;
            break;
        case 'P':
            password = optarg;
            break;
        default:
            std::cerr << "Usage: luxtronik2mqtt [-h host] [-p port] [-q qos] [-t topic] [-i id] [-u username] [-P password]" << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* register signal handler */
    abortLoop = false;
    signal(SIGTERM, signalHandler);

    /* mosquitto constructor */
    if (mosqpp::lib_init() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_init failed" << std::endl;
        return EXIT_FAILURE;
    }

    /* start MqttClient */
    MqttClient * mqttClient = new MqttClient(host.c_str(), port, qos, topic.c_str(), id.c_str(), username.c_str(), password.c_str(), "#");

    /* start loop */
    while(hauptMenue(mqttClient)) {}

    /* delete resources */
    delete mqttClient;

    /* mosquitto destructor */
    if (mosqpp::lib_cleanup() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_cleanup failed" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
