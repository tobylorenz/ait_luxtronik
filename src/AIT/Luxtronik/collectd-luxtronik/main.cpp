/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

/* C includes */
#include <unistd.h>
#ifdef WITH_SYSTEMD
#include <systemd/sd-daemon.h>
#endif

/* C++ includes */
#include <atomic>
#include <chrono>
#include <csignal>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <mosquittopp.h>
#include <thread>

/* project internal includes */
#include "MqttClient.h"

/** abort the main loop */
std::atomic<bool> abortLoop;

/** handle SIGTERM */
void signalHandler(int /*signum*/)
{
    abortLoop = true;
}

/** main function */
int main(int argc, char ** argv)
{
    /* default parameters */
    std::string host = "localhost";
    int port = 1883;
    int qos = 1;
    std::string topic = "heizung/luxtronik";
    std::string id = "collectd-luxtronik";
    std::string username = "";
    std::string password = "";
    std::string collectd_identifier = "heizung/luxtronik"; // host "/" plugin ["-" plugin instance]

    /* evaluate command line parameters */
    int c;
    while ((c = getopt (argc, argv, "h:p:q:t:i:u:P:d:c:")) != -1) {
        switch (c) {
        case 'h':
            host = optarg;
            break;
        case 'p':
            port = std::stoul(optarg);
            break;
        case 'q':
            qos = std::stoul(optarg);
            break;
        case 't':
            topic = optarg;
            break;
        case 'i':
            id = optarg;
        case 'u':
            username = optarg;
            break;
        case 'P':
            password = optarg;
            break;
        case 'c':
            collectd_identifier = optarg;
            break;
        default:
            std::cerr << "Usage: luxtronik2mqtt [-h host] [-p port] [-q qos] [-t topic] [-i id] [-u username] [-P password] [-c collectd-identifier]" << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* register signal handler */
    abortLoop = false;
    signal(SIGTERM, signalHandler);

    /* mosquitto constructor */
    if (mosqpp::lib_init() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_init failed" << std::endl;
        return EXIT_FAILURE;
    }

    /* start MqttClient */
    MqttClient * mqttClient = new MqttClient(host.c_str(), port, qos, topic.c_str(), id.c_str(), username.c_str(), password.c_str(), "#");

#ifdef WITH_SYSTEMD
    /* systemd notify */
    sd_notify(0, "READY=1");
#endif

    /* start subscribe loop */
    std::chrono::time_point<std::chrono::system_clock> tp = std::chrono::system_clock::now();
    while (!abortLoop) {
        /* sleep */
        tp += std::chrono::seconds(10);
        std::this_thread::sleep_until(tp);

#ifdef WITH_SYSTEMD
        /* systemd notify */
        sd_notify(0, "WATCHDOG=1");
#endif

        /* read topics and write to collectd */
        bool ready = mqttClient->getTopic("$state") == "ready";
        if (ready) {
            /* 1100 */
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_2 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/2", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_3 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/3", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_4 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/4", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_5 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/5", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_6 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/6", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_7 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/7", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_8 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/8", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_9 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/9", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_10 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/10", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_11 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/11", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_12 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/12", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_13 interval=10 N:"
                      << mqttClient->getTopic("physical/1100/13", "U") << std::endl;
            /* 1200 */
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_2 interval=10 N:"
                      << mqttClient->getTopic("raw/1200/2", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_3 interval=10 N:"
                      << mqttClient->getTopic("raw/1200/3", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_4 interval=10 N:"
                      << mqttClient->getTopic("raw/1200/4", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_5 interval=10 N:"
                      << mqttClient->getTopic("raw/1200/5", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_6 interval=10 N:"
                      << mqttClient->getTopic("raw/1200/6", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_7 interval=10 N:"
                      << mqttClient->getTopic("raw/1200/7", "U") << std::endl;
            /* 1300 */
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_2 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/2", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_3 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/3", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_4 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/4", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_5 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/5", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_6 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/6", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_7 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/7", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_8 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/8", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_9 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/9", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_10 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/10", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_11 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/11", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_12 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/12", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_13 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/13", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_14 interval=10 N:"
                      << mqttClient->getTopic("raw/1300/14", "U") << std::endl;
            /* 1450 */
            std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_2 interval=10 N:"
                      << mqttClient->getTopic("raw/1450/2", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_3 interval=10 N:"
                      << mqttClient->getTopic("raw/1450/3", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/duration-raw_1450_4 interval=10 N:"
                      << mqttClient->getTopic("raw/1450/4", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_8 interval=10 N:"
                      << mqttClient->getTopic("raw/1450/8", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_9 interval=10 N:"
                      << mqttClient->getTopic("raw/1450/9", "U") << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_10 interval=10 N:"
                      << mqttClient->getTopic("raw/1450/10", "U") << std::endl;
        } else {
            /* Note: When submitting U to a counter the behavior is undefined. */
            /* 1100 */
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_2 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_3 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_4 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_5 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_6 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_7 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_8 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_9 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_10 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_11 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_12 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/temperature-physical_1100_13 interval=10 N:U" << std::endl;
            /* 1200 */
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_2 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_3 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_4 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_5 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_6 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1200_7 interval=10 N:U" << std::endl;
            /* 1300 */
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_2 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_3 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_4 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_5 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_6 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_7 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_8 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_9 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_10 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_11 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_12 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_13 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/gauge-raw_1300_14 interval=10 N:U" << std::endl;
            /* 1450 */
            //std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_2 interval=10 N:U" << std::endl;
            //std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_3 interval=10 N:U" << std::endl;
            std::cout << "PUTVAL " << collectd_identifier << "/duration-raw_1450_4 interval=10 N:U" << std::endl;
            //std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_8 interval=10 N:U" << std::endl;
            //std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_9 interval=10 N:U" << std::endl;
            //std::cout << "PUTVAL " << collectd_identifier << "/counter-raw_1450_10 interval=10 N:"
        }
    }

    /* delete resources */
    delete mqttClient;

    /* mosquitto destructor */
    if (mosqpp::lib_cleanup() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_cleanup failed" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
