#!/bin/sh

BINARY=/home/users/tobias/bitbucket/build-AIT_Luxtronik-Desktop-Debug/src/AIT/Luxtronik/luxtronik-simulator/luxtronik-simulator
USER=tobias
DEVICEFILE=/home/users/tobias/.wine/dosdevices/com1
PORT=9445 # kundendienst passwort :-)

# FD:mode=0666
# NAMED=
# PTY=link=$DEVICEFILE,wait-slave
# TERMIOS=echo=1,icanon=0

rm -f $DEVICEFILE
socat -v \
    PTY,link=$DEVICEFILE,wait-slave,mode=0666,b57600,min=1,time=0,ignbrk=1,brkint=0,icrnl=0,imaxbel=0,opost=0,onlcr=0,isig=0,icanon=0,iexten=0,echo=0,echoe=0,echok=0,echoctl=0,echoke=0 \
    EXEC:"$BINARY",pty
