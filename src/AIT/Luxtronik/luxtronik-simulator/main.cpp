/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

/* C++ includes */
#include <iostream>
#include <map>
#include <string>

/** main function */
int main(int /*argc*/, char ** /*argv*/)
{
    /* define initial channels */
    static std::map<std::string, std::string> channels = {
        // Informationen
        { "1100", "1100;12;290;288;150;295;318;411;420;164;164;750;0;0" },
        { "1200", "1200;6;0;1;0;1;1;0" },
        { "1300", "1300;13;0;0;1;1;0;1;0;0;0;0;0;0;0" },
        { "1400", "1400;29;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;3;57;0;0;0;98;47;52;0;0;0;0;0;0" },
        { "1450", "1450;9;43025322;57223;751;0;0;0;1470210;3088125;43025322" },
        { "1500;1500", "1500;1500;6;715;1;2;18;18;29" },
        { "1500;1501", "1500;1501;6;716;2;2;18;3;5" },
        { "1500;1502", "1500;1502;6;715;3;2;18;2;25" },
        { "1500;1503", "1500;1503;6;716;8;5;18;5;35" },
        { "1500;1504", "1500;1504;6;715;8;5;18;10;21" },
        { "1600;1600", "1600;1600;6;010;21;7;18;19;36" },
        { "1600;1601", "1600;1601;6;010;21;7;18;23;51" },
        { "1600;1602", "1600;1602;6;010;22;7;18;4;7" },
        { "1600;1603", "1600;1603;6;010;22;7;18;9;3" },
        { "1600;1604", "1600;1604;6;010;22;7;18;11;47" },
        { "1700", "1700;12;11; R2.33;1;5;14;7;18;19;2;16,1,1" },
        // Einstellungen
        { "2100", "2100;16;500;20;30;50;-160;100;540;20;500;350;-200;-90;1300;20;-200;580" },
        { "2200", "2200;30;0;0;0;0;1;2;1;2;0;0;1;1;0;0;0;0;15;0;0;0;1;1;0;0;0;0;0;0;100;100" },
        { "2300", "2300;4;0;0;0;0" },
        { "2400", "2400;3;2;1;3" },
        { "2500", "2500;9;0;0;0;0;1;0;3600;3600;300" },
        { "2600", "2600;1;0" },
        { "2700", "2700;7;18;7;22;0;12;51;37" },
        { "2800", "2800;1;2" },
        // Schaltzeiten
        { "3100", "3100;8;0;0;0;0;0;0;0;0" },
        { "3130", "3130;8;0;0;0;0;0;0;0;0" },
        { "3150;0", "3150;0;8;0;0;0;0;0;0;0;0" },
        { "3150;1", "3150;1;8;0;0;0;0;0;0;0;0" },
        { "3150;2", "3150;2;8;0;0;0;0;0;0;0;0" },
        { "3150;3", "3150;3;8;0;0;0;0;0;0;0;0" },
        { "3150;4", "3150;4;8;0;0;0;0;0;0;0;0" },
        { "3150;5", "3150;5;8;0;0;0;0;0;0;0;0" },
        { "3150;6", "3150;6;8;0;0;0;0;0;0;0;0" },
        { "3160", "3160;8;0;0;0;0;0;0;0;0" },
        { "3180;0", "3180;0;8;0;0;0;0;0;0;0;0" },
        { "3180;1", "3180;1;8;0;0;0;0;0;0;0;0" },
        { "3180;2", "3180;2;8;0;0;0;0;0;0;0;0" },
        { "3180;3", "3180;3;8;0;0;0;0;0;0;0;0" },
        { "3180;4", "3180;4;8;0;0;0;0;0;0;0;0" },
        { "3180;5", "3180;5;8;0;0;0;0;0;0;0;0" },
        { "3180;6", "3180;6;8;0;0;0;0;0;0;0;0" },
        { "3200", "3200;8;0;0;0;0;0;0;0;0" },
        { "3220;0", "3220;0;8;0;0;0;0;0;0;0;0" },
        { "3220;1", "3220;1;8;0;0;0;0;0;0;0;0" },
        { "3220;2", "3220;2;8;0;0;0;0;0;0;0;0" },
        { "3220;3", "3220;3;8;0;0;0;0;0;0;0;0" },
        { "3220;4", "3220;4;8;0;0;0;0;0;0;0;0" },
        { "3220;5", "3220;5;8;0;0;0;0;0;0;0;0" },
        { "3220;6", "3220;6;8;0;0;0;0;0;0;0;0" },
        // Heizung
        { "3400", "3400;9;0;380;220;0;650;420;200;0;350" },
        { "3405", "3405;1;0" },
        { "3410", "3410;23;1;0;0;1;0;1;0;0;150;288;0;1;0;0;288;750;0;98;0;0;0;0;1" },
        // Brauchwasser
        { "3500", "3500;17;0;0;1;0;420;411;0;0;0;0;0;0;1300;200;0;426;0" },
        { "3505", "3505;1;0" },
        { "3600", "3600;9;0;0;0;0;0;0;0;0;0" }, // @todo 3600
        // Auswerten
        // @todo 4100
        // @todo 4200
        // Compact
        { "5100", "5100;2;0;0" },
        { "5200", "5200;2;0;0" },
        { "5300", "5300;7;0;0;0;0;0;0;0" },
        { "5400", "5400;10;0;0;0;0;0;0;0;0;0;0" },
        { "5600", "5600;16;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16" },
        { "5620;0", "5620;0;16;1;2;3;4;5;6;7;8;1;2;3;4;5;6;7;8" },
        { "5620;1", "5620;1;16;1;2;3;4;5;6;7;8;1;2;3;4;5;6;7;8" },
        { "5620;2", "5620;2;16;1;2;3;4;5;6;7;8;1;2;3;4;5;6;7;8" },
        { "5620;3", "5620;3;16;1;2;3;4;5;6;7;8;1;2;3;4;5;6;7;8" },
        { "5620;4", "5620;4;16;1;2;3;4;5;6;7;8;1;2;3;4;5;6;7;8" },
        { "5620;5", "5620;5;16;1;2;3;4;5;6;7;8;1;2;3;4;5;6;7;8" },
        { "5620;6", "5620;6;16;1;2;3;4;5;6;7;8;1;2;3;4;5;6;7;8" },
        // Comfort
        { "6100", "6100;4;750;36;115;50" },
        { "6200", "6200;1;1" },
        { "6300", "6300;6;0;0;0;0;0;0" },
        { "6400", "6400;6;0;0;0;0;0;0" },
        { "6410", "6410;9;7;6;5;4;3;2;1;50;50" },
        { "6500", "6500;4;1;2;3;0" },
        { "6540;0", "6540;0;8;1;2;3;4;5;6;7;8" },
        { "6540;1", "6540;1;8;1;2;3;4;5;6;7;8" },
        { "6540;2", "6540;2;8;1;2;3;4;5;6;7;8" },
        { "6540;3", "6540;3;8;1;2;3;4;5;6;7;8" },
        { "6540;4", "6540;4;8;1;2;3;4;5;6;7;8" },
        { "6540;5", "6540;5;8;1;2;3;4;5;6;7;8" },
        { "6540;6", "6540;6;8;1;2;3;4;5;6;7;8" },
    };

    /* Luxtronik command loop */
    for(;;) {
        /* getline */
        std::string line;
        std::getline(std::cin, line);
        while ((line.front() == '\r') || (line.front() == '\n')) {
            line.erase(0, 1);
        }
        while ((line.back() == '\r') || (line.back() == '\n')) {
            line.pop_back();
        }

        /* interpret and respond */
        if (line == "") {
            // ignore empty lines
        } else
        if (line == "0") {
            return EXIT_SUCCESS;
        } else
        if (line == "ATE0") {
            // echo disable
            std::cout << "OK" << std::endl;
        } else
        if (line == "ATL1") {
            // set speaker to low
            std::cout << "OK" << std::endl;
        } else
        if (line == "ATL0") {
            // set speaker to mute
            std::cout << "OK" << std::endl;
        } else
        if (line == "AT0") {
            // ?
            std::cout << "OK" << std::endl;
        } else
        if (line == "ATX4") {
            // ?
            std::cout << "OK" << std::endl;
        } else
        if (line == "ATD") {
            // dial
            std::cout << "CONNECT" << std::endl;
            std::cout << "AIT-Diagnose" << std::endl;
        } else
        if (line == "1500") {
            /* send */
            std::cout << "1500;5" << std::endl;
            std::cout << channels["1500;1500"] << std::endl;
            std::cout << channels["1500;1501"] << std::endl;
            std::cout << channels["1500;1502"] << std::endl;
            std::cout << channels["1500;1503"] << std::endl;
            std::cout << channels["1500;1504"] << std::endl;
            std::cout << "1500;5\r" << std::endl;
        } else
        if (line == "1600") {
            /* send */
            std::cout << "1600;5" << std::endl;
            std::cout << channels["1600;1600"] << std::endl;
            std::cout << channels["1600;1601"] << std::endl;
            std::cout << channels["1600;1602"] << std::endl;
            std::cout << channels["1600;1603"] << std::endl;
            std::cout << channels["1600;1604"] << std::endl;
            std::cout << "1600;5\r" << std::endl;
        } else
        if (line == "1800") {
            /* send */
            std::cout << "1800;8" << std::endl;
            std::cout << channels["1100"] << std::endl;
            std::cout << channels["1200"] << std::endl;
            std::cout << channels["1300"] << std::endl;
            std::cout << channels["1400"] << std::endl;
            std::cout << channels["1450"] << std::endl;
            std::cout << "1500;5" << std::endl;
            std::cout << channels["1500;1500"] << std::endl;
            std::cout << channels["1500;1501"] << std::endl;
            std::cout << channels["1500;1502"] << std::endl;
            std::cout << channels["1500;1503"] << std::endl;
            std::cout << channels["1500;1504"] << std::endl;
            std::cout << "1500;5\r" << std::endl;
            std::cout << "1600;5" << std::endl;
            std::cout << channels["1600;1600"] << std::endl;
            std::cout << channels["1600;1601"] << std::endl;
            std::cout << channels["1600;1602"] << std::endl;
            std::cout << channels["1600;1603"] << std::endl;
            std::cout << channels["1600;1604"] << std::endl;
            std::cout << "1600;5\r" << std::endl;
            std::cout << channels["1700"] << std::endl;
            std::cout << "1800;8\r" << std::endl;
        } else
        if (line == "3150") {
            /* send */
            std::cout << "3150;7" << std::endl;
            std::cout << channels["3150;0"] << std::endl;
            std::cout << channels["3150;1"] << std::endl;
            std::cout << channels["3150;2"] << std::endl;
            std::cout << channels["3150;3"] << std::endl;
            std::cout << channels["3150;4"] << std::endl;
            std::cout << channels["3150;5"] << std::endl;
            std::cout << channels["3150;6"] << std::endl;
            std::cout << "3150;7\r" << std::endl;
        } else
        if (line == "3180") {
            /* send */
            std::cout << "3180;7" << std::endl;
            std::cout << channels["3180;0"] << std::endl;
            std::cout << channels["3180;1"] << std::endl;
            std::cout << channels["3180;2"] << std::endl;
            std::cout << channels["3180;3"] << std::endl;
            std::cout << channels["3180;4"] << std::endl;
            std::cout << channels["3180;5"] << std::endl;
            std::cout << channels["3180;6"] << std::endl;
            std::cout << "3180;7\r" << std::endl;
        } else
        if (line == "3220") {
            /* send */
            std::cout << "3220;7" << std::endl;
            std::cout << channels["3220;0"] << std::endl;
            std::cout << channels["3220;1"] << std::endl;
            std::cout << channels["3220;2"] << std::endl;
            std::cout << channels["3220;3"] << std::endl;
            std::cout << channels["3220;4"] << std::endl;
            std::cout << channels["3220;5"] << std::endl;
            std::cout << channels["3220;6"] << std::endl;
            std::cout << "3220;7\r" << std::endl;
        } else
        if (line == "5500") {
            /* send */
            std::cout << "5500;4" << std::endl;
            std::cout << channels["5100"] << std::endl;
            std::cout << channels["5200"] << std::endl;
            std::cout << channels["5300"] << std::endl;
            std::cout << channels["5400"] << std::endl;
            std::cout << "5500;4\r" << std::endl;
        } else
        if (line == "5620") {
            /* send */
            std::cout << "5620;7" << std::endl;
            std::cout << channels["5620;0"] << std::endl;
            std::cout << channels["5620;1"] << std::endl;
            std::cout << channels["5620;2"] << std::endl;
            std::cout << channels["5620;3"] << std::endl;
            std::cout << channels["5620;4"] << std::endl;
            std::cout << channels["5620;5"] << std::endl;
            std::cout << channels["5620;6"] << std::endl;
            std::cout << "5620;7\r" << std::endl;
        } else
        if (line == "6540") {
            /* send */
            std::cout << "6540;7" << std::endl;
            std::cout << channels["6540;0"] << std::endl;
            std::cout << channels["6540;1"] << std::endl;
            std::cout << channels["6540;2"] << std::endl;
            std::cout << channels["6540;3"] << std::endl;
            std::cout << channels["6540;4"] << std::endl;
            std::cout << channels["6540;5"] << std::endl;
            std::cout << channels["6540;6"] << std::endl;
            std::cout << "6540;7\r" << std::endl;
        } else
        if (channels.count(line) > 0) {
            /* send */
            std::cout << channels[line] << std::endl;
        } else
        {
            /* send error code */
            std::cout << "777" << std::endl;
        }
    }

    return EXIT_SUCCESS;
}
