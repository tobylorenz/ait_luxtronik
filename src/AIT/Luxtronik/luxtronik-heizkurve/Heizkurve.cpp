/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Heizkurve.h"

/* C++ includes */
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>

Heizkurve::Heizkurve(std::string filename) :
    m_filename(filename),
    m_heizkurve(),
    m_lastAussenIst(0.0),
    m_lastRuecklaufSoll(0.0),
    m_lastUpdate(std::chrono::high_resolution_clock::now())
{
    readFile();
}

Heizkurve::~Heizkurve()
{
}

void Heizkurve::setPoint(double aussenIst, double ruecklaufSoll)
{
    /* check for updates */
    if ((m_lastAussenIst != aussenIst) || (m_lastRuecklaufSoll != ruecklaufSoll)) {
        m_lastUpdate = std::chrono::high_resolution_clock::now();
        m_lastAussenIst = aussenIst;
        m_lastRuecklaufSoll = ruecklaufSoll;
    }

    /* check if last point was stable for more than 10 seconds */
    auto stableTime =
        std::chrono::high_resolution_clock::now() - m_lastUpdate;
    if (stableTime >= std::chrono::seconds(10)) {
        if ((m_heizkurve.count(m_lastAussenIst) == 0) || (m_heizkurve[m_lastAussenIst] != m_lastRuecklaufSoll)) {
            m_heizkurve[m_lastAussenIst] = m_lastRuecklaufSoll;
            if ((m_lastAussenIst != 0) && (m_lastRuecklaufSoll != 0)) {
                writeFile();
            }
        }
    }
}

void Heizkurve::readFile()
{
    std::ifstream ifs(m_filename);
    if (!ifs.is_open()) {
        return;
    }

    std::string line;
    while(std::getline(ifs, line)) {
        std::istringstream iss(line);

        std::string first;
        std::getline(iss, first, ' ');
        if (first != "#") {
            std::string second;
            std::getline(iss, second, ' ');

            m_heizkurve[std::stod(first)] = std::stod(second);
        }
    }

    ifs.close();
}

void Heizkurve::writeFile()
{
    std::ofstream ofs(m_filename);
    if (!ofs.is_open()) {
        return;
    }

    ofs << "# Aussen-Ist Rücklauf-Soll" << std::endl;
    for(const auto & keyValue: m_heizkurve) {
        ofs << std::fixed << std::setprecision(1) << keyValue.first << " "
            << std::fixed << std::setprecision(1) << keyValue.second << std::endl;
    }

    ofs.close();
}
