/*
 * Copyright (C) 2018 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

/* C++ includes */
#include <chrono>
#include <map>
#include <string>

class Heizkurve
{
public:
    Heizkurve(std::string filename);
    virtual ~Heizkurve();

    /** set point */
    void setPoint(double aussenIst, double ruecklaufSoll);

private:
    /** Filename */
    std::string m_filename;

    /** Heizkurve (Aussen-Ist, Rücklauf-Soll) */
    std::map<double, double> m_heizkurve;

    /** last update time */
    double m_lastAussenIst;
    double m_lastRuecklaufSoll;
    std::chrono::time_point<std::chrono::high_resolution_clock> m_lastUpdate;

    /** read Heizkurve */
    void readFile();

    /** write Heizkurve */
    void writeFile();
};
