/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

/* C includes */
#include <unistd.h>
#ifdef WITH_SYSTEMD
#include <systemd/sd-daemon.h>
#endif

/* C++ includes */
#include <atomic>
#include <chrono>
#include <csignal>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <mosquittopp.h>
#include <thread>

/* project internal includes */
#include "Heizkurve.h"
#include "MqttClient.h"

/** abort the main loop */
std::atomic<bool> abortLoop;

/** handle SIGTERM */
void signalHandler(int /*signum*/)
{
    abortLoop = true;
}

/** main function */
int main(int argc, char ** argv)
{
    /* default parameters */
    std::string host = "localhost";
    int port = 1883;
    int qos = 1;
    std::string topic = "heizung/luxtronik";
    std::string id = "luxtronik-heizkurve";
    std::string username = "";
    std::string password = "";

    /* evaluate command line parameters */
    int c;
    while ((c = getopt (argc, argv, "h:p:q:t:i:u:P:d:c:")) != -1) {
        switch (c) {
        case 'h':
            host = optarg;
            break;
        case 'p':
            port = std::stoul(optarg);
            break;
        case 'q':
            qos = std::stoul(optarg);
            break;
        case 't':
            topic = optarg;
            break;
        case 'i':
            id = optarg;
        case 'u':
            username = optarg;
            break;
        case 'P':
            password = optarg;
            break;
        default:
            std::cerr << "Usage: luxtronik2mqtt [-h host] [-p port] [-q qos] [-t topic] [-i id] [-u username] [-P password]" << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* register signal handler */
    abortLoop = false;
    signal(SIGTERM, signalHandler);

    /* mosquitto constructor */
    if (mosqpp::lib_init() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_init failed" << std::endl;
        return EXIT_FAILURE;
    }

    /* start heizkurve */
    Heizkurve * heizkurve = new Heizkurve("/var/lib/luxtronik/heizkurve.dat");

    /* start MqttClient */
    MqttClient * mqttClient = new MqttClient(host.c_str(), port, qos, topic.c_str(), id.c_str(), username.c_str(), password.c_str(), "physical/1100/+");

#ifdef WITH_SYSTEMD
    /* systemd notify */
    sd_notify(0, "READY=1");
#endif

    /* start subscribe loop */
    std::chrono::time_point<std::chrono::system_clock> tp = std::chrono::system_clock::now();
    while(!abortLoop) {
        /* sleep */
        tp += std::chrono::seconds(1);
        std::this_thread::sleep_until(tp);

#ifdef WITH_SYSTEMD
        /* systemd notify */
        sd_notify(0, "WATCHDOG=1");
#endif

        try {
            heizkurve->setPoint(
                std::stod(mqttClient->getTopic("physical/1100/6")),
                std::stod(mqttClient->getTopic("physical/1100/4")));
        } catch (const std::exception &) {
        }
    }

    /* delete resources */
    delete mqttClient;
    delete heizkurve;

    /* mosquitto destructor */
    if (mosqpp::lib_cleanup() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_cleanup failed" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
