/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel3410.h"

Channel3410::Channel3410(MqttClient * mqttClient) :
    ChannelBase("3410", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Weitere Informationen");

    mqttClient->setTopic("physical/" + m_id + "/2/$name", "aktuelle Priorität");

    mqttClient->setTopic("physical/" + m_id + "/3/$name", "aktuelle Betriebsart");

    mqttClient->setTopic("physical/" + m_id + "/4/$name", "Regelung");

    mqttClient->setTopic("physical/" + m_id + "/5/$name", "Schaltuhr Heizung");

    mqttClient->setTopic("physical/" + m_id + "/6/$name", "HAN");

    mqttClient->setTopic("physical/" + m_id + "/7/$name", "HUP");

    mqttClient->setTopic("physical/" + m_id + "/8/$name", "WAN");

    mqttClient->setTopic("physical/" + m_id + "/9/$name", "ZAN");

    mqttClient->setTopic("physical/" + m_id + "/10/$name", "Rücklauf-Soll");
    mqttClient->setTopic("physical/" + m_id + "/10/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/11/$name", "Rücklauf-Ist");
    mqttClient->setTopic("physical/" + m_id + "/11/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/12/$name", "Zwangsheizung");

    mqttClient->setTopic("physical/" + m_id + "/13/$name", "Schaltuhr Mischkreis 1");

    mqttClient->setTopic("physical/" + m_id + "/14/$name", "Mischkreis 1 Typ");

    mqttClient->setTopic("physical/" + m_id + "/15/$name", "MK1-Vorlauf-Soll");
    mqttClient->setTopic("physical/" + m_id + "/15/$unit", "°C");

    // @todo mqttClient->setTopic("physical/" + m_id + "/16/$name", "???");

    mqttClient->setTopic("physical/" + m_id + "/17/$name", "MK1-Vorlauf-Ist");
    mqttClient->setTopic("physical/" + m_id + "/17/$unit", "°C");

    // @todo mqttClient->setTopic("physical/" + m_id + "/18/$name", "???");

    mqttClient->setTopic("physical/" + m_id + "/19/$name", "Periode Lademischer");

    mqttClient->setTopic("physical/" + m_id + "/20/$name", "Puls Lademischer");

    mqttClient->setTopic("physical/" + m_id + "/21/$name", "Periode Entlademischer");

    mqttClient->setTopic("physical/" + m_id + "/22/$name", "Puls Entlademischer");

    // @todo mqttClient->setTopic("physical/" + m_id + "/23/$name", "???");

    mqttClient->setTopic("physical/" + m_id + "/24/$name", "Mischer");
}

void Channel3410::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", getItem(2));

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Automatik" },
            { 1, "ZWE" },
            { 2, "Party" },
            { 3, "Ferien" },
            { 4, "Aus" }
        };
        uint8_t itemValue = std::stoul(getItem(3));
        m_mqttClient->setTopic("physical/" + m_id + "/3", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/3", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/4", (getItem(4) == "0") ? "Aussent." : "Festt.");
    m_mqttClient->setTopic("physical/" + m_id + "/5", (getItem(5) == "0") ? "Anhebung" : "Absenkung");

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "neutral" },
            { 1, "mehr" },
            { 2, "weniger" },
        };
        uint8_t itemValue = std::stoul(getItem(6));
        m_mqttClient->setTopic("physical/" + m_id + "/6", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/6", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/7", (getItem(7) == "0") ? "Aus" : "Ein");
    m_mqttClient->setTopic("physical/" + m_id + "/8", (getItem(8) == "0") ? "Aus" : "Ein");
    m_mqttClient->setTopic("physical/" + m_id + "/9", (getItem(9) == "0") ? "Aus" : "Ein");

    m_mqttClient->setTopic("physical/" + m_id + "/10", rawToPhysical(getItem(10), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/11", rawToPhysical(getItem(11), 0.1, 1));

    m_mqttClient->setTopic("physical/" + m_id + "/12", (getItem(12) == "0") ? "Aus" : "Ein");
    m_mqttClient->setTopic("physical/" + m_id + "/13", (getItem(13) == "0") ? "Anhebung" : "Absenkung");

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Entlade" },
            { 2, "Lade" },
            { 3, "Kühl" }
        };
        uint8_t itemValue = std::stoul(getItem(14));
        m_mqttClient->setTopic("physical/" + m_id + "/14", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/14", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/15", rawToPhysical(getItem(15), 0.1, 1));

    // @todo 3410/16

    m_mqttClient->setTopic("physical/" + m_id + "/17", rawToPhysical(getItem(17), 0.1, 1));

    // @todo 3410/18

    m_mqttClient->setTopic("physical/" + m_id + "/19", getItem(19));
    m_mqttClient->setTopic("physical/" + m_id + "/20", getItem(20));
    m_mqttClient->setTopic("physical/" + m_id + "/21", getItem(21));
    m_mqttClient->setTopic("physical/" + m_id + "/22", getItem(22));

    // @todo 3410/23

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "neutral" },
            { 1, "Zu" },
            { 2, "Auf" }
        };
        uint8_t itemValue = std::stoul(getItem(24));
        m_mqttClient->setTopic("physical/" + m_id + "/24", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/24", "");
    }
}
