/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel5600.h"

Channel5600::Channel5600(MqttClient * mqttClient) :
    ChannelBase("5600", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Schaltzeiten Woche");

    m_mqttClient->setTopic("physical/" + m_id + "/2-3/$name", "Tag Anfang 1");
    m_mqttClient->setTopic("physical/" + m_id + "/4-5/$name", "Tag Ende 1");
    m_mqttClient->setTopic("physical/" + m_id + "/6-7/$name", "Tag Anfang 2");
    m_mqttClient->setTopic("physical/" + m_id + "/8-9/$name", "Tag Ende 2");

    m_mqttClient->setTopic("physical/" + m_id + "/2-3/$name", "Nacht Anfang 1");
    m_mqttClient->setTopic("physical/" + m_id + "/4-5/$name", "Nacht Ende 1");
    m_mqttClient->setTopic("physical/" + m_id + "/6-7/$name", "Nacht Anfang 2");
    m_mqttClient->setTopic("physical/" + m_id + "/8-9/$name", "Nacht Ende 2");
}

void Channel5600::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2-3", hmsToTime(getItem(2), getItem(3), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/4-5", hmsToTime(getItem(4), getItem(5), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/6-7", hmsToTime(getItem(6), getItem(7), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/8-9", hmsToTime(getItem(8), getItem(9), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/10-11", hmsToTime(getItem(10), getItem(11), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/12-13", hmsToTime(getItem(12), getItem(13), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/14-15", hmsToTime(getItem(14), getItem(15), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/16-17", hmsToTime(getItem(16), getItem(17), "0"));
}
