/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "SubChannel4100.h"

SubChannel4100::SubChannel4100(std::string id, MqttClient * mqttClient) :
    ChannelBase(id, mqttClient)
{
    m_mqttClient->setTopic("physical/4100/" + m_id + "/2-4/$name", "Datum");
    m_mqttClient->setTopic("physical/4100/" + m_id + "/5-6/$name", "Uhrzeit");

    m_mqttClient->setTopic("physical/4100/" + m_id + "/7/$name", "Vorlauf"); // TVL
    m_mqttClient->setTopic("physical/4100/" + m_id + "/7/$unit", "°C");

    m_mqttClient->setTopic("physical/4100/" + m_id + "/8/$name", "Rücklauf"); // TRL
    m_mqttClient->setTopic("physical/4100/" + m_id + "/8/$unit", "°C");

    m_mqttClient->setTopic("physical/4100/" + m_id + "/9/$name", "WQ-Ein"); // TWE
    m_mqttClient->setTopic("physical/4100/" + m_id + "/9/$unit", "°C");

    m_mqttClient->setTopic("physical/4100/" + m_id + "/10/$name", "WQ-Aus"); // TWA
    m_mqttClient->setTopic("physical/4100/" + m_id + "/10/$unit", "°C");

    m_mqttClient->setTopic("physical/4100/" + m_id + "/11/$name", "Heißgas"); // THG
    m_mqttClient->setTopic("physical/4100/" + m_id + "/11/$unit", "°C");

    m_mqttClient->setTopic("physical/4100/" + m_id + "/12/$name", "BW-Ist"); // TBW
    m_mqttClient->setTopic("physical/4100/" + m_id + "/12/$unit", "°C");

    m_mqttClient->setTopic("physical/4100/" + m_id + "/13/$name", "MK1-Vorlauf"); // TB1
    m_mqttClient->setTopic("physical/4100/" + m_id + "/13/$unit", "°C");

    m_mqttClient->setTopic("physical/4100/" + m_id + "/14/$name", "Aussentemp."); // TA
    m_mqttClient->setTopic("physical/4100/" + m_id + "/14/$unit", "°C");
}

void SubChannel4100::mqttPublishRaw()
{
    /* line */
    m_mqttClient->setTopic("raw/4100/" + m_id, m_line);

    /* list elements */
    for (uint8_t i = 2; i < m_list.size(); i++) {
        m_mqttClient->setTopic("raw/4100/" + m_id + "/" + std::to_string(i), m_list[i]);
    }
}

void SubChannel4100::mqttPublishPhysical()
{
    m_mqttClient->setTopic("raw/4100/" + m_id + "/2-4", ymdToDate(getItem(4), getItem(3), getItem(2)));
    m_mqttClient->setTopic("raw/4100/" + m_id + "/5-6", hmsToTime(getItem(5), getItem(6), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/7", rawToPhysical(getItem(7), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/8", rawToPhysical(getItem(8), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/9", rawToPhysical(getItem(9), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/10", rawToPhysical(getItem(10), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/11", rawToPhysical(getItem(11), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/12", rawToPhysical(getItem(12), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/13", rawToPhysical(getItem(13), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/14", rawToPhysical(getItem(14), 0.1, 1));
}
