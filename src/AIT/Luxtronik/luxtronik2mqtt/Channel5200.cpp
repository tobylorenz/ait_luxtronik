/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel5200.h"

Channel5200::Channel5200(MqttClient * mqttClient) :
    ChannelBase("5200", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Eingänge");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Stosslüftung Extern");
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "");
}

void Channel5200::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", (getItem(2) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/3", (getItem(3) == "0") ? "AUS" : "EIN");
}
