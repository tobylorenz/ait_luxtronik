/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1500.h"

/* C++ includes */
#include <iostream>

Channel1500::Channel1500(MqttClient * mqttClient) :
    ChannelBase("1500", mqttClient),
    channel1500_1500("1500", mqttClient),
    channel1500_1501("1501", mqttClient),
    channel1500_1502("1502", mqttClient),
    channel1500_1503("1503", mqttClient),
    channel1500_1504("1504", mqttClient)
{
}

void Channel1500::mqttPublishRaw()
{
    channel1500_1500.mqttPublishRaw();
    channel1500_1501.mqttPublishRaw();
    channel1500_1502.mqttPublishRaw();
    channel1500_1503.mqttPublishRaw();
    channel1500_1504.mqttPublishRaw();
}

void Channel1500::mqttPublishPhysical()
{
    channel1500_1500.mqttPublishPhysical();
    channel1500_1501.mqttPublishPhysical();
    channel1500_1502.mqttPublishPhysical();
    channel1500_1503.mqttPublishPhysical();
    channel1500_1504.mqttPublishPhysical();
}
