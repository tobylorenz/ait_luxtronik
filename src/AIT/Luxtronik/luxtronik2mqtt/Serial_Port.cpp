/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Serial_Port.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <string>

Serial_Port::Serial_Port(boost::asio::io_context & io_context):
    m_serial_port(io_context),
    m_write_buffer(),
    m_read_buffer(),
    m_timeout(boost::posix_time::milliseconds(100)),
    m_timeout_timer(io_context),
    m_delay_timer(io_context),
    m_log_file()
{
}

Serial_Port::~Serial_Port()
{
}

void Serial_Port::open(const std::string & deviceName, const unsigned int baud_rate)
{
    m_serial_port.open(deviceName);
    m_serial_port.set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
    m_serial_port.set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));
    m_serial_port.set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
    m_serial_port.set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
    m_serial_port.set_option(boost::asio::serial_port_base::character_size(8));

    /* initiate read */
    async_read(); // read
    async_wait_timeout(); // timeout
}

bool Serial_Port::is_open() const
{
    return m_serial_port.is_open();
}

void Serial_Port::close()
{
    m_serial_port.close();
}

void Serial_Port::write_request(const std::string request)
{
    m_write_buffer = request + "\r\n";

    /* initiate write */
    async_write(); // request
    async_wait_timeout(); // timeout
}

void Serial_Port::delay(boost::posix_time::time_duration duration)
{
    /* initiate delay */
    async_wait_delay(duration); // delay
}

void Serial_Port::open_log_file(const std::string & filename)
{
    m_log_file.open(filename, std::ios_base::out);
}

void Serial_Port::close_log_file()
{
    m_log_file.close();
}

void Serial_Port::async_write()
{
    boost::asio::async_write(
        m_serial_port,
        boost::asio::buffer(m_write_buffer),
        std::bind(&Serial_Port::write_completed, this, std::placeholders::_1, std::placeholders::_2));
}

void Serial_Port::write_completed(const boost::system::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch(error.value()) {
    case 0: // success (data written)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    if (m_log_file.is_open()) {
        m_log_file << "<" << m_write_buffer << std::endl;
    }
}

void Serial_Port::async_read()
{
    boost::asio::async_read_until(
        m_serial_port,
        boost::asio::dynamic_buffer(m_read_buffer),
        "\r\n",
        std::bind(&Serial_Port::read_completed, this, std::placeholders::_1, std::placeholders::_2));
}

void Serial_Port::read_completed(const boost::system::error_code & error, const size_t /*bytes_transferred*/)
{
    switch(error.value()) {
    case 0: // success (read completed)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    /* stop timer */
    m_timeout_timer.cancel();

    /* parse data */
    while(!m_read_buffer.empty()) {
        size_t delim_pos = m_read_buffer.find("\r\n");
        if (delim_pos == std::string::npos)
            break;
        const std::string line = m_read_buffer.substr(0, delim_pos);
        m_read_buffer.erase(0, delim_pos + 2);

        if (m_log_file.is_open()) {
            m_log_file << ">" << line << std::endl;
        }

        /* run handler */
        if (line.empty()) {
            // ignore
        } else
        if (line + "\r\n" == m_write_buffer) {
            if (request_echo_handler) {
                request_echo_handler(line);
            }
        } else {
            if (response_handler) {
                response_handler(line);
            }
        }
    }

    /* initiate next read */
    async_read(); // echo
    async_wait_timeout(); // timeout
}

void Serial_Port::async_wait_timeout()
{
    m_timeout_timer.expires_from_now(m_timeout);
    m_timeout_timer.async_wait(
        std::bind(&Serial_Port::timeout_timer_expired, this, std::placeholders::_1));
}

void Serial_Port::timeout_timer_expired(const boost::system::error_code & error)
{
    switch(error.value()) {
    case 0: // success (timer expired)
        break;
    case boost::asio::error::operation_aborted: // timer cancelled
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* run handler */
    if (timeout_handler) {
        timeout_handler();
    }
}

void Serial_Port::async_wait_delay(boost::posix_time::time_duration duration)
{
    m_delay_timer.expires_from_now(duration);
    m_delay_timer.async_wait(
        std::bind(&Serial_Port::delay_timer_expired, this, std::placeholders::_1));
}

void Serial_Port::delay_timer_expired(const boost::system::error_code & error)
{
    switch(error.value()) {
    case 0: // success (timer expired)
        break;
    case boost::asio::error::operation_aborted: // timer cancelled
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* run handler */
    if (delay_handler) {
        delay_handler();
    }
}
