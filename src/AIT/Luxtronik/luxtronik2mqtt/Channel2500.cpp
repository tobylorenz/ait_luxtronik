/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel2500.h"

Channel2500::Channel2500(MqttClient * mqttClient) :
    ChannelBase("2500", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Entlüften");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "HUP");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "BUP");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "Venti BOSUP");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "ZUP/ZIP");

    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "Laufzeit");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$unit", "h");

    // @todo m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "?"); (0)
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/8/$name", "?"); (3600s=1h)
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/9/$name", "?"); // (3600s=1h)
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/10/$name", "?"); // (300s=5min)
}

void Channel2500::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", (getItem(2) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/3", (getItem(3) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/4", (getItem(4) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/5", (getItem(5) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/6", getItem(8));
    // @todo 2500/7 (0)
    m_mqttClient->setTopic("physical/" + m_id + "/8", secondsToHMS(getItem(8)));
    m_mqttClient->setTopic("physical/" + m_id + "/9", secondsToHMS(getItem(9)));
    m_mqttClient->setTopic("physical/" + m_id + "/10", secondsToHMS(getItem(10)));
}
