/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1800.h"

/* C++ includes */
#include <iostream>

Channel1800::Channel1800(MqttClient * mqttClient) :
    ChannelBase("1800", mqttClient),
    channel1100(mqttClient),
    channel1200(mqttClient),
    channel1300(mqttClient),
    channel1400(mqttClient),
    channel1450(mqttClient),
    channel1500(mqttClient),
    channel1600(mqttClient),
    channel1700(mqttClient)
{
}

void Channel1800::mqttPublishRaw()
{
    channel1100.mqttPublishRaw();
    channel1200.mqttPublishRaw();
    channel1300.mqttPublishRaw();
    channel1400.mqttPublishRaw();
    channel1450.mqttPublishRaw();
    channel1500.mqttPublishRaw();
    channel1600.mqttPublishRaw();
    channel1700.mqttPublishRaw();
}

void Channel1800::mqttPublishPhysical()
{
    channel1100.mqttPublishPhysical();
    channel1200.mqttPublishPhysical();
    channel1300.mqttPublishPhysical();
    channel1400.mqttPublishPhysical();
    channel1450.mqttPublishPhysical();
    channel1500.mqttPublishPhysical();
    channel1600.mqttPublishPhysical();
    channel1700.mqttPublishPhysical();
}
