/*
 * Copyright (C) 2018 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "SubChannel4200.h"

SubChannel4200::SubChannel4200(std::string id, SerialPort * serialPort, MqttClient * mqttClient) :
    ChannelBase(id, serialPort, mqttClient)
{
    // @todo 4200
}

void SubChannel4200::mqttPublishRaw()
{
    /* line */
    m_mqttClient->setTopic("luxtronik/raw/4200/" + m_id, m_line);

    /* list elements */
    for (uint8_t i = 2; i < m_list.size(); i++) {
        m_mqttClient->setTopic("luxtronik/raw/4200/" + m_id + "/" + std::to_string(i), m_list[i]);
    }
}

void SubChannel4200::mqttPublishPhysical()
{
    // @todo 4200
}
