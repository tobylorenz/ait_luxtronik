/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

/* C includes */
#include <unistd.h>
#ifdef WITH_SYSTEMD
#include <systemd/sd-daemon.h>
#endif

/* C++ includes */
#include <array>
#include <atomic>
#include <chrono>
#include <csignal>
#include <cstdint>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <mosquittopp.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

/* project internal includes */
#include "Channel1800.h"
#include "Channel2100.h"
#include "Channel2200.h"
#include "Channel2300.h"
#include "Channel2400.h"
#include "Channel2500.h"
#include "Channel2600.h"
#include "Channel2700.h"
#include "Channel2800.h"
#include "Channel3100.h"
#include "Channel3200.h"
#include "Channel3400.h"
#include "Channel3405.h"
#include "Channel3410.h"
#include "Channel3500.h"
#include "Channel3505.h"
#include "Channel3600.h"
#include "Serial_Port.h"
#include "MqttClient.h"

/** abort the main loop */
std::atomic<bool> abortLoop;

/** handle SIGTERM */
void signalHandler(int /*signum*/)
{
    abortLoop = true;
}

/** main function */
int main(int argc, char ** argv)
{
    /* default parameters */
    std::string host = "localhost";
    int port = 1883;
    int qos = 1;
    std::string topic = "heizung/luxtronik";
    std::string id = "luxtronik2mqtt";
    std::string username = "";
    std::string password = "";
    std::string device = "/dev/waermepumpe";
    std::string logFileName = "";
    int sleeptime = 2;

    /* evaluate command line parameters */
    int c;
    while ((c = getopt(argc, argv, "h:p:q:t:i:u:P:d:s:l:")) != -1) {
        switch (c) {
        case 'h':
            host = optarg;
            break;
        case 'p':
            port = std::stoul(optarg);
            break;
        case 'q':
            qos = std::stoul(optarg);
            break;
        case 't':
            topic = optarg;
            break;
        case 'i':
            id = optarg;
        case 'u':
            username = optarg;
            break;
        case 'P':
            password = optarg;
            break;
        case 'd':
            device = optarg;
            break;
        case 's':
            sleeptime = std::stoul(optarg);
            break;
        case 'l':
            logFileName = optarg;
            break;
        default:
            std::cerr << "Usage: luxtronik2mqtt [-h host] [-p port] [-q qos] [-t topic] [-i id] [-u username] [-P password] [-d device] [-s sleeptime] [-l logFileName]" << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* register signal handler */
    abortLoop = false;
    signal(SIGTERM, signalHandler);

    /* mosquitto constructor */
    if (mosqpp::lib_init() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_init failed" << std::endl;
        return EXIT_FAILURE;
    }

    /* io context */
    boost::asio::io_context io_context;
    auto work = boost::asio::make_work_guard(io_context);

    /* start serial port */
    Serial_Port * serial_port = new Serial_Port(io_context);
    if (!logFileName.empty()) {
        serial_port->open_log_file(logFileName);
    }

    /* start MqttClient */
    MqttClient * mqttClient = new MqttClient(host.c_str(), port, qos, topic.c_str(), id.c_str(), username.c_str(), password.c_str(), "#/set");

    /* init all channels */
    Channel1800 channel1800(mqttClient);
    Channel2100 channel2100(mqttClient);
    Channel2200 channel2200(mqttClient);
    Channel2300 channel2300(mqttClient);
    Channel2400 channel2400(mqttClient);
    Channel2500 channel2500(mqttClient);
    Channel2600 channel2600(mqttClient);
    Channel2700 channel2700(mqttClient);
    Channel2800 channel2800(mqttClient);
    Channel3100 channel3100(mqttClient);
    Channel3200 channel3200(mqttClient);
    Channel3400 channel3400(mqttClient);
    Channel3405 channel3405(mqttClient);
    Channel3410 channel3410(mqttClient);
    Channel3500 channel3500(mqttClient);
    Channel3505 channel3505(mqttClient);
    Channel3600 channel3600(mqttClient);

#ifdef WITH_SYSTEMD
    /* systemd notify */
    sd_notify(0, "READY=1");
#endif

    /* states */
    enum State {
        // Init / Recover
        Init,
        // 1800
        Wait_for_1800_Start,
        Wait_for_1800_1100,
        Wait_for_1800_1200,
        Wait_for_1800_1300,
        Wait_for_1800_1400,
        Wait_for_1800_1450,
        Wait_for_1800_1500_Start,
        Wait_for_1800_1500_1500,
        Wait_for_1800_1500_1501,
        Wait_for_1800_1500_1502,
        Wait_for_1800_1500_1503,
        Wait_for_1800_1500_1504,
        Wait_for_1800_1500_End,
        Wait_for_1800_1600_Start,
        Wait_for_1800_1600_1600,
        Wait_for_1800_1600_1601,
        Wait_for_1800_1600_1602,
        Wait_for_1800_1600_1603,
        Wait_for_1800_1600_1604,
        Wait_for_1800_1600_End,
        Wait_for_1800_1700,
        Wait_for_1800_End,
        // 2100
        Wait_for_2100,
        // 2200
        Wait_for_2200,
        // 2300
        Wait_for_2300,
        // 2400
        Wait_for_2400,
        // 2500
        Wait_for_2500,
        // 2600
        Wait_for_2600,
        // 2700
        Wait_for_2700,
        // 2800
        Wait_for_2800,
        // 3100
        Wait_for_3100,
        // 3200
        Wait_for_3200,
        // 3400
        Wait_for_3400,
        // 3405
        Wait_for_3405,
        // 3410
        Wait_for_3410,
        // 3500
        Wait_for_3500,
        // 3505
        Wait_for_3505,
        // 3600
        Wait_for_3600
    } state = State::Init;

    /* loop timer */
    auto startTime = std::chrono::high_resolution_clock::now();

    /* setup handlers */
    serial_port->request_echo_handler = [&](std::string /*request*/){
        // ignore
    };
    serial_port->response_handler = [&](std::string response){
        switch(state) {
        case Init:
            // ignore any response during init to recover
            state = State::Init;
            break;
        case Wait_for_1800_Start:
            if (response == "1800;8") {
                state = State::Wait_for_1800_1100;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1100:
            if (response.rfind("1100;", 0) == 0) {
                channel1800.channel1100.setLine(response);
                state = State::Wait_for_1800_1200;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1200:
            if (response.rfind("1200;", 0) == 0) {
                channel1800.channel1200.setLine(response);
                state = State::Wait_for_1800_1300;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1300:
            if (response.rfind("1300;", 0) == 0) {
                channel1800.channel1300.setLine(response);
                state = State::Wait_for_1800_1400;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1400:
            if (response.rfind("1400;", 0) == 0) {
                channel1800.channel1400.setLine(response);
                state = State::Wait_for_1800_1450;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1450:
            if (response.rfind("1450;", 0) == 0) {
                channel1800.channel1450.setLine(response);
                state = State::Wait_for_1800_1500_Start;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1500_Start:
            if (response == "1500;5") {
                state = State::Wait_for_1800_1500_1500;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1500_1500:
            if (response.rfind("1500;1500;", 0) == 0) {
                channel1800.channel1500.channel1500_1500.setLine(response);
                state = State::Wait_for_1800_1500_1501;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1500_1501:
            if (response.rfind("1500;1501;", 0) == 0) {
                channel1800.channel1500.channel1500_1501.setLine(response);
                state = State::Wait_for_1800_1500_1502;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1500_1502:
            if (response.rfind("1500;1502;", 0) == 0) {
                channel1800.channel1500.channel1500_1502.setLine(response);
                state = State::Wait_for_1800_1500_1503;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1500_1503:
            if (response.rfind("1500;1503;", 0) == 0) {
                channel1800.channel1500.channel1500_1503.setLine(response);
                state = State::Wait_for_1800_1500_1504;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1500_1504:
            if (response.rfind("1500;1504;", 0) == 0) {
                channel1800.channel1500.channel1500_1504.setLine(response);
                state = State::Wait_for_1800_1500_End;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1500_End:
            if (response == "1500;5\r") {
                state = State::Wait_for_1800_1600_Start;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1600_Start:
            if (response == "1600;5") {
                state = State::Wait_for_1800_1600_1600;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1600_1600:
            if (response.rfind("1600;1600;", 0) == 0) {
                channel1800.channel1600.channel1600_1600.setLine(response);
                state = State::Wait_for_1800_1600_1601;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1600_1601:
            if (response.rfind("1600;1601;", 0) == 0) {
                channel1800.channel1600.channel1600_1601.setLine(response);
                state = State::Wait_for_1800_1600_1602;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1600_1602:
            if (response.rfind("1600;1602;", 0) == 0) {
                channel1800.channel1600.channel1600_1602.setLine(response);
                state = State::Wait_for_1800_1600_1603;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1600_1603:
            if (response.rfind("1600;1603;", 0) == 0) {
                channel1800.channel1600.channel1600_1603.setLine(response);
                state = State::Wait_for_1800_1600_1604;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1600_1604:
            if (response.rfind("1600;1604;", 0) == 0) {
                channel1800.channel1600.channel1600_1604.setLine(response);
                state = State::Wait_for_1800_1600_End;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1600_End:
            if (response == "1600;5\r") {
                state = State::Wait_for_1800_1700;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_1700:
            if (response.rfind("1700;", 0) == 0) {
                channel1800.channel1700.setLine(response);
                state = State::Wait_for_1800_End;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_1800_End:
            if (response == "1800;8\r") {
                serial_port->write_request("2100");
                state = Wait_for_2100;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_2100:
            if (response.rfind("2100;", 0) == 0) {
                channel2100.setLine(response);
                serial_port->write_request("2200");
                state = Wait_for_2200;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_2200:
            if (response.rfind("2200;", 0) == 0) {
                channel2200.setLine(response);
                serial_port->write_request("2300");
                state = Wait_for_2300;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_2300:
            if (response.rfind("2300;", 0) == 0) {
                channel2300.setLine(response);
                serial_port->write_request("2400");
                state = Wait_for_2400;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_2400:
            if (response.rfind("2400;", 0) == 0) {
                channel2400.setLine(response);
                serial_port->write_request("2500");
                state = Wait_for_2500;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_2500:
            if (response.rfind("2500;", 0) == 0) {
                channel2500.setLine(response);
                serial_port->write_request("2600");
                state = Wait_for_2600;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_2600:
            if (response.rfind("2600;", 0) == 0) {
                channel2600.setLine(response);
                serial_port->write_request("2700");
                state = Wait_for_2700;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_2700:
            if (response.rfind("2700;", 0) == 0) {
                channel2700.setLine(response);
                serial_port->write_request("2800");
                state = Wait_for_2800;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_2800:
            if (response.rfind("2800;", 0) == 0) {
                channel2800.setLine(response);
                serial_port->write_request("3100");
                state = Wait_for_3100;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_3100:
            if (response.rfind("3100;", 0) == 0) {
                channel3100.setLine(response);
                serial_port->write_request("3200");
                state = Wait_for_3200;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_3200:
            if (response.rfind("3200;", 0) == 0) {
                channel3200.setLine(response);
                serial_port->write_request("3400");
                state = Wait_for_3400;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_3400:
            if (response.rfind("3400;", 0) == 0) {
                channel3400.setLine(response);
                serial_port->write_request("3405");
                state = Wait_for_3405;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_3405:
            if (response.rfind("3405;", 0) == 0) {
                channel3405.setLine(response);
                serial_port->write_request("3410");
                state = Wait_for_3410;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_3410:
            if (response.rfind("3410;", 0) == 0) {
                channel3410.setLine(response);
                serial_port->write_request("3500");
                state = Wait_for_3500;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_3500:
            if (response.rfind("3500;", 0) == 0) {
                channel3500.setLine(response);
                serial_port->write_request("3505");
                state = Wait_for_3505;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_3505:
            if (response.rfind("3505;", 0) == 0) {
                channel3505.setLine(response);
                serial_port->write_request("3600");
                state = Wait_for_3600;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        case Wait_for_3600:
            if (response.rfind("3600;", 0) == 0) {
                channel3600.setLine(response);
                auto stopTime = std::chrono::high_resolution_clock::now();
                auto diffTime = stopTime - startTime;
                mqttClient->setTopic("$loopTimeMs", std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(diffTime).count()));
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            } else {
                serial_port->delay(boost::posix_time::seconds(sleeptime));
                state = State::Init;
            }
            break;
        }
    };
    serial_port->timeout_handler = [&](){
        serial_port->delay(boost::posix_time::seconds(sleeptime));
        state = State::Init;
    };
    serial_port->delay_handler = [&](){
        startTime = std::chrono::high_resolution_clock::now();
        serial_port->write_request("1800");
        state = Wait_for_1800_Start;
    };

    /* open serial port and initiate */
    serial_port->open(device.c_str());

    /* run execution handler */
    while(!abortLoop) {
        io_context.run_one();
    }

    /* delete resources */
    delete mqttClient;
    delete serial_port;

    /* mosquitto destructor */
    if (mosqpp::lib_cleanup() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_cleanup failed" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
