/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1600.h"

/* C++ includes */
#include <iostream>

Channel1600::Channel1600(MqttClient * mqttClient) :
    ChannelBase("1600", mqttClient),
    channel1600_1600("1600", mqttClient),
    channel1600_1601("1601", mqttClient),
    channel1600_1602("1602", mqttClient),
    channel1600_1603("1603", mqttClient),
    channel1600_1604("1604", mqttClient)
{
}

void Channel1600::mqttPublishRaw()
{
    channel1600_1600.mqttPublishRaw();
    channel1600_1601.mqttPublishRaw();
    channel1600_1602.mqttPublishRaw();
    channel1600_1603.mqttPublishRaw();
    channel1600_1604.mqttPublishRaw();
}

void Channel1600::mqttPublishPhysical()
{
    channel1600_1600.mqttPublishPhysical();
    channel1600_1601.mqttPublishPhysical();
    channel1600_1602.mqttPublishPhysical();
    channel1600_1603.mqttPublishPhysical();
    channel1600_1604.mqttPublishPhysical();
}
