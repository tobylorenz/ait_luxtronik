/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel3160.h"

Channel3160::Channel3160(MqttClient * mqttClient) :
    ChannelBase("3160", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Heizung Mischkreis 1 Woche");

    m_mqttClient->setTopic("physical/" + m_id + "/2-3/$name", "Anheben 1");
    m_mqttClient->setTopic("physical/" + m_id + "/4-5/$name", "Absenken 1");
    m_mqttClient->setTopic("physical/" + m_id + "/6-7/$name", "Anheben 2");
    m_mqttClient->setTopic("physical/" + m_id + "/8-9/$name", "Absenken 2");
}

void Channel3160::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2-3", hmsToTime(getItem(2), getItem(3), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/4-5", hmsToTime(getItem(4), getItem(5), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/6-7", hmsToTime(getItem(6), getItem(7), "0"));
    m_mqttClient->setTopic("physical/" + m_id + "/8-9", hmsToTime(getItem(8), getItem(9), "0"));
}
