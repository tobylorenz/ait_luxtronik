/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel3505.h"

Channel3505::Channel3505(MqttClient * mqttClient) :
    ChannelBase("3505", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Brauchwasser Betriebsart");
}

void Channel3505::mqttPublishPhysical()
{
    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Automatik" },
            { 1, "Zweit.-Wärmeerz." },
            { 2, "Party" },
            { 3, "Ferien" },
            { 4, "Aus" }
        };
        uint8_t itemValue = std::stoul(getItem(2));
        m_mqttClient->setTopic("physical/" + m_id + "/2", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/2", "");
    }
}
