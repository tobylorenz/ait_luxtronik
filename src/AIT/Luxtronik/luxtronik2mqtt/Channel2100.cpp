/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel2100.h"

Channel2100::Channel2100(MqttClient * mqttClient) :
    ChannelBase("2100", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Temperaturen");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Rückl.-Begr.");
    m_mqttClient->setTopic("physical/" + m_id + "/2/$range", "35.0:70.0");
    m_mqttClient->setTopic("physical/" + m_id + "/2/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Hysterese HR");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$range", "0.5:3.0");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$unit", "K");

    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "TR Erh max");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$range", "1.0:7.0");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$unit", "K");

    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "Freig. 2.VD");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$range", "-20.0:20.0");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "Freig. ZWE");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$range", "-20.0:20.0");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "T-Luftabt.");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$range", "0.0:20.0");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/8/$name", "TDI-Solltemp");
    m_mqttClient->setTopic("physical/" + m_id + "/8/$range", "50.0:70.0");
    m_mqttClient->setTopic("physical/" + m_id + "/8/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/9/$name", "Hysterese BW");
    m_mqttClient->setTopic("physical/" + m_id + "/9/$range", "1.0:30.0");
    m_mqttClient->setTopic("physical/" + m_id + "/9/$unit", "K");

    m_mqttClient->setTopic("physical/" + m_id + "/10/$name", "Vorl 2.VD BW");
    m_mqttClient->setTopic("physical/" + m_id + "/10/$range", "10.0:70.0");
    m_mqttClient->setTopic("physical/" + m_id + "/10/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/11/$name", "TAussen max");
    m_mqttClient->setTopic("physical/" + m_id + "/11/$range", "10.0:45.0");
    m_mqttClient->setTopic("physical/" + m_id + "/11/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/12/$name", "TAussen min");
    m_mqttClient->setTopic("physical/" + m_id + "/12/$range", "-20.0:10.0");
    m_mqttClient->setTopic("physical/" + m_id + "/12/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/13/$name", "T-WQ min");
    m_mqttClient->setTopic("physical/" + m_id + "/13/$range", "-20.0:10.0");
    m_mqttClient->setTopic("physical/" + m_id + "/13/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/14/$name", "T-HG max");
    m_mqttClient->setTopic("physical/" + m_id + "/14/$range", "90.0:140.0");
    m_mqttClient->setTopic("physical/" + m_id + "/14/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/15/$name", "T-LABT-Ende");
    m_mqttClient->setTopic("physical/" + m_id + "/15/$range", "2.0:10.0");
    m_mqttClient->setTopic("physical/" + m_id + "/15/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/16/$name", "Absenk. bis");
    m_mqttClient->setTopic("physical/" + m_id + "/16/$range", "-20.0:10.0");
    m_mqttClient->setTopic("physical/" + m_id + "/16/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/17/$name", "Vorlauf max.");
    m_mqttClient->setTopic("physical/" + m_id + "/17/$range", "35.0:70.0");
    m_mqttClient->setTopic("physical/" + m_id + "/17/$unit", "°C");
}

void Channel2100::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", rawToPhysical(getItem(2), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/3", rawToPhysical(getItem(3), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/4", rawToPhysical(getItem(4), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/5", rawToPhysical(getItem(5), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/6", rawToPhysical(getItem(6), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/7", rawToPhysical(getItem(7), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/8", rawToPhysical(getItem(8), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/9", rawToPhysical(getItem(9), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/10", rawToPhysical(getItem(10), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/11", rawToPhysical(getItem(11), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/12", rawToPhysical(getItem(12), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/13", rawToPhysical(getItem(13), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/14", rawToPhysical(getItem(14), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/15", rawToPhysical(getItem(15), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/16", rawToPhysical(getItem(16), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/17", rawToPhysical(getItem(17), 0.1, 1));
}
