/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "SubChannel1500.h"

SubChannel1500::SubChannel1500(std::string id, MqttClient * mqttClient) :
    ChannelBase(id, mqttClient)
{
    m_mqttClient->setTopic("physical/1500/" + m_id + "/3/$name", "Code");
    m_mqttClient->setTopic("physical/1500/" + m_id + "/4-6/$name", "Datum");
    m_mqttClient->setTopic("physical/1500/" + m_id + "/7-8/$name", "Uhrzeit");
}

void SubChannel1500::mqttPublishRaw()
{
    /* line */
    m_mqttClient->setTopic("raw/1500/" + m_id, m_line);

    /* list elements */
    for (uint8_t i = 3; i < m_list.size(); i++) {
        m_mqttClient->setTopic("raw/1500/" + m_id + "/" + std::to_string(i), m_list[i]);
    }
}

void SubChannel1500::mqttPublishPhysical()
{
    try {
        static const std::map<uint16_t, std::string> itemTexts = {
            { 700, "----" },
            { 701, "NDS" },     // Niederdruckstörung
            { 702, "NEG" },     // Niederdrucksperre
            { 703, "S-FRO" },   // Frostschutz
            { 704, "S-HG" },    // Heissgasstörung
            { 705, "S-MOT" },   // Motorschutz VEN
            { 706, "S-MOT" },   // Motorschutz BSUP
            { 707, "S-CW" },    // Kodierung Wärmepumpe
            { 708, "S-TRL" },   // Fühler Rücklauf
            { 709, "S-TVL" },   // Fühler Vorlauf
            { 710, "S-THG" },   // Fühler Heissgas
            { 711, "S-TA" },    // Fühler Aussentemperatur
            { 712, "S-TBW" },   // Fühler Brauchwasser
            { 713, "S-TWE" },   // Fühler WQ-Ein
            { 714, "BAN2" },    // Heissgas BW
            { 715, "HDA" },     // Hochdruckabschaltung
            { 716, "HDS" },     // Hochdruckstörung
            { 717, "S-DFS" },   // Durchfluss WQ
            { 718, "TEGMAX" },  // Max. Aussentemperatur
            { 719, "TEGMIN" },  // Min. Aussentemperatur
            { 720, "UEG" },     // WQ-Temperatur
            { 721, "----" },    // "NDAB" Niederdruckabschaltung
            { 722, "S-TDHZ" },  // Temperaturdiffenz HW
            { 723, "TD-BW" },   // Temperaturdiffenz BW
            { 724, "TD-ABT" },  // Temperaturdiffenz ABT
            { 725, "S-BW" },    // Anlagenfehler BW
            { 726, "STFB1" },   // Fühler Mischkreis 1
            { 727, "S-SDP" },   // Soledruck
            { 728, "S-TWA" },   // Fühler WQ-Aus
            { 729, "---" },     // "S-VÜW" Drehfeldfehler
            { 730, "S-AHP" },   // Leistung Ausheizung
            { 731, "----" },
            { 732, "S-KKP" },   // Störung Kühlung
            { 733, "S-PEX" },   // Störung Anode
            { 734, "S-PEX" },   // Störung Anodes
            { 735, "S-TEE" },   // Fühler ext.En
            { 736, "S-TSK" },   // Fühler Solarkollektor
            { 737, "S-TSS" },   // Fehler Solarspeicher
            { 738, "S-TFB2" },  // Fehler Mischkreis 2
            { 739, "----" },    // S-CAN1
            { 740, "----" },    // S-CAN2
            { 741, "----" },    // S-CAN3
            { 742, "----" },    // S-CAN4
            { 743, "----" },    // S-CAN5
            { 744, "----" },
            { 745, "S-MOD" },   // Modemfehler
            { 746, "----" },
            { 747, "----" },
            { 748, "----" },
            { 749, "----" },
            { 750, "----" },    // "S-TRL-E" Fühler Rücklauf extern
            { 751, "----" },    // "S-PH-V" Phasenüberwachungsfehler
            { 752, "----" }     // "S-PH DFS" Phasenüberwachungs/-Durchflussfehler
        };
        uint16_t itemValue = std::stoul(getItem(3));
        m_mqttClient->setTopic("physical/1500/" + m_id + "/3", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/1500/" + m_id + "/3", "");
    }
    m_mqttClient->setTopic("physical/1500/" + m_id + "/4-6", ymdToDate(getItem(6), getItem(5), getItem(4)));
    m_mqttClient->setTopic("physical/1500/" + m_id + "/7-8", hmsToTime(getItem(7), getItem(8), "0"));
}
