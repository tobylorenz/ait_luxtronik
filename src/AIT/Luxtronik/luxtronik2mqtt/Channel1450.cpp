/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1450.h"

Channel1450::Channel1450(MqttClient * mqttClient) :
    ChannelBase("1450", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Betriebsstd.");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "BStd VD1");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Imp. VD1");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "d.EZ VD1");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "BStd VD2");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "Imp. VD2");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "d.EZ VD2");
    m_mqttClient->setTopic("physical/" + m_id + "/8/$name", "BStd ZWE1");
    m_mqttClient->setTopic("physical/" + m_id + "/9/$name", "BStd ZWE2");
    m_mqttClient->setTopic("physical/" + m_id + "/10/$name", "BStd WP");
}

void Channel1450::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", secondsToHMS(getItem(2)));
    m_mqttClient->setTopic("physical/" + m_id + "/3", getItem(3));
    m_mqttClient->setTopic("physical/" + m_id + "/4", secondsToHMS(getItem(4)));
    m_mqttClient->setTopic("physical/" + m_id + "/5", secondsToHMS(getItem(5)));
    m_mqttClient->setTopic("physical/" + m_id + "/6", getItem(6));
    m_mqttClient->setTopic("physical/" + m_id + "/7", secondsToHMS(getItem(7)));
    m_mqttClient->setTopic("physical/" + m_id + "/8", secondsToHMS(getItem(8)));
    m_mqttClient->setTopic("physical/" + m_id + "/9", secondsToHMS(getItem(9)));
    m_mqttClient->setTopic("physical/" + m_id + "/10", secondsToHMS(getItem(10)));
}
