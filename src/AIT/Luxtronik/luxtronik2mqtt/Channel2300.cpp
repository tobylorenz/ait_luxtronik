/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel2300.h"

Channel2300::Channel2300(MqttClient * mqttClient) :
    ChannelBase("2300", mqttClient)
{
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/$name", "???"); // Kurzprogramme?

    // @todo 2300/2-5 (0;0;0;0)
    // 2300/2: Kurzprogramme
    // 2300/3: Zwangsheizung
    // 2300/4: Zwangsbrauchwasser
    // 2300/5: Abtauen
}

void Channel2300::mqttPublishPhysical()
{
    // @todo 2300/2-5
}
