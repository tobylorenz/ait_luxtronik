/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel2400.h"

Channel2400::Channel2400(MqttClient * mqttClient) :
    ChannelBase("2400", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Prioritäten");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Heizung");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Brauchwasser");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "Schwimmbad");
}

void Channel2400::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", getItem(2));
    m_mqttClient->setTopic("physical/" + m_id + "/3", getItem(3));
    m_mqttClient->setTopic("physical/" + m_id + "/4", getItem(4));
}
