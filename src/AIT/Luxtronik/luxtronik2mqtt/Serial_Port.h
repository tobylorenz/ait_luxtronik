/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <functional>
#include <fstream>

#include <boost/utility.hpp>
#include <boost/asio.hpp>

/** serial port */
class Serial_Port: private boost::noncopyable
{
public:
    Serial_Port(boost::asio::io_context & io_context);
    virtual ~Serial_Port();

    /** open */
    virtual void open(const std::string & deviceName, const unsigned int baud_rate = 57600);

    /** is open */
    virtual bool is_open() const;

    /** close */
    virtual void close();

    /** write request */
    void write_request(const std::string request);

    /** delay */
    void delay(boost::posix_time::time_duration duration);

    /** request echo handler */
    std::function<void(const std::string request)> request_echo_handler;

    /** response handler */
    std::function<void(const std::string response)> response_handler;

    /** delay handler */
    std::function<void()> delay_handler;

    /** timeout handler */
    std::function<void()> timeout_handler;

    /** open log file */
    virtual void open_log_file(const std::string & fileName);

    /** close log file */
    virtual void close_log_file();

private:
    /** serial port handle */
    boost::asio::serial_port m_serial_port;

    /** write buffer */
    std::string m_write_buffer;

    /** asynchronous write */
    virtual void async_write();

    /** write completed */
    virtual void write_completed(const boost::system::error_code & error, std::size_t bytes_transferred);

    /** read buffer */
    std::string m_read_buffer;

    /** asynchronous read */
    virtual void async_read();

    /** read completed */
    virtual void read_completed(const boost::system::error_code & error, std::size_t bytes_transferred);

    /** timeout duration */
    boost::posix_time::time_duration m_timeout;

    /** timeout timer */
    boost::asio::deadline_timer m_timeout_timer;

    /** asynchronous timeout timer wait */
    virtual void async_wait_timeout();

    /** timer expired */
    virtual void timeout_timer_expired(const boost::system::error_code & error);

    /** delay timer */
    boost::asio::deadline_timer m_delay_timer;

    /** asynchronous delay timer wait */
    virtual void async_wait_delay(boost::posix_time::time_duration duration);

    /** timer expired */
    virtual void delay_timer_expired(const boost::system::error_code & error);

    /** log file */
    std::ofstream m_log_file;
};
