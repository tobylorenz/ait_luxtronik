/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel4100.h"

/* C++ includes */
#include <iostream>

Channel4100::Channel4100(MqttClient * mqttClient) :
    ChannelBase("4100", mqttClient),
    channel4100_1("1", mqttClient),
    channel4100_2("2", mqttClient),
    channel4100_3("3", mqttClient),
    channel4100_4("4", mqttClient),
    channel4100_5("5", mqttClient)
    // @todo up to 2879
{
}

void Channel4100::mqttPublishRaw()
{
    channel4100_1.mqttPublishRaw();
    channel4100_2.mqttPublishRaw();
    channel4100_3.mqttPublishRaw();
    channel4100_4.mqttPublishRaw();
    channel4100_5.mqttPublishRaw();
    // @todo up to 2879
}

void Channel4100::mqttPublishPhysical()
{
    channel4100_1.mqttPublishPhysical();
    channel4100_2.mqttPublishPhysical();
    channel4100_3.mqttPublishPhysical();
    channel4100_4.mqttPublishPhysical();
    channel4100_5.mqttPublishPhysical();
    // @todo up to 2879
}
