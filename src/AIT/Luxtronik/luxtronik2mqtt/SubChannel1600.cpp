/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "SubChannel1600.h"

SubChannel1600::SubChannel1600(std::string id, MqttClient * mqttClient) :
    ChannelBase(id, mqttClient)
{
    m_mqttClient->setTopic("physical/1600/" + m_id + "/3/$name", "Code");
    m_mqttClient->setTopic("physical/1600/" + m_id + "/4-6/$name", "Datum");
    m_mqttClient->setTopic("physical/1600/" + m_id + "/7-8/$name", "Uhrzeit");
}

void SubChannel1600::mqttPublishRaw()
{
    /* line */
    m_mqttClient->setTopic("raw/1600/" + m_id, m_line);

    /* list elements */
    for (uint8_t i = 3; i < m_list.size(); i++) {
        m_mqttClient->setTopic("raw/1600/" + m_id + "/" + std::to_string(i), m_list[i]);
    }
}

void SubChannel1600::mqttPublishPhysical()
{
    try {
        static const std::map<uint16_t, std::string> itemTexts = {
            { 0, "---" },
            { 1, "WPS" }, // Wärmepumpenstörung
            { 2, "ANS" }, // Anlagenstörung
            { 3, "" }, // Umschaltung auf Betriebsart ZWE
            { 4, "EVU" }, // EVU-Sperre
            { 5, "" }, // entfällt
            { 6, "" }, // Luftabtauung
            { 7, "" }, // TEG max
            { 8, "" }, // TEG min
            { 9, "" }, // UEG
            { 10, "W.W." }, // Weniger Wärme (keine Anforderung)
            { 11, "" } // Fehlernummer noch nicht bekannt
        };
        uint16_t itemValue = std::stoul(getItem(3));
        m_mqttClient->setTopic("physical/1600/" + m_id + "/3", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/1600/" + m_id + "/3", "");
    }
    m_mqttClient->setTopic("physical/1600/" + m_id + "/4-6", ymdToDate(getItem(6), getItem(5), getItem(4)));
    m_mqttClient->setTopic("physical/1600/" + m_id + "/7-8", hmsToTime(getItem(7), getItem(8), "0"));
}
