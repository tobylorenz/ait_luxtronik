/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1200.h"

Channel1200::Channel1200(MqttClient * mqttClient) :
    ChannelBase("1200", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Eingänge");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "ASD");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "EVU");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "HD");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "MOT");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "ND");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "PEX");
}

void Channel1200::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", (getItem(2) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/3", (getItem(3) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/4", (getItem(4) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/5", (getItem(5) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/6", (getItem(6) == "0") ? "AUS" : "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/7", (getItem(7) == "0") ? "AUS" : "EIN");
}
