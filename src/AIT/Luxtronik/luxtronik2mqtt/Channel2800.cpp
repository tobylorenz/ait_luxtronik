/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel2800.h"

#include <array>

Channel2800::Channel2800(MqttClient * mqttClient) :
    ChannelBase("2800", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Anlagenkonfiguration");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Anlagenkonfiguration");
}

void Channel2800::mqttPublishPhysical()
{
    try {
        static const std::array<std::string, 7> itemTexts = {
            "Heizung",
            "Brauchwasser",
            "Heizung+Brauchwasser",
            "Schwimmbad",
            "Heizung+Schwimmbad",
            "Brauchwasser+Schwimmbad",
            "Heizung+Brauchwasser+Schwimmbad"
        };
        uint8_t itemValue = std::stoul(getItem(2));
        m_mqttClient->setTopic("physical/" + m_id + "/2", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/2", "");
    }
}
