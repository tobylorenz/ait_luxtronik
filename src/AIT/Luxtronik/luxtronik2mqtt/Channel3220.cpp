/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel3220.h"

/* C++ includes */
#include <iostream>

Channel3220::Channel3220(MqttClient * mqttClient) :
    ChannelBase("3220", mqttClient),
    channel3220_0("0", mqttClient),
    channel3220_1("1", mqttClient),
    channel3220_2("2", mqttClient),
    channel3220_3("3", mqttClient),
    channel3220_4("4", mqttClient),
    channel3220_5("5", mqttClient),
    channel3220_6("6", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Schaltzeiten Tage - Brauchwasser");
}

void Channel3220::mqttPublishRaw()
{
    channel3220_0.mqttPublishRaw();
    channel3220_1.mqttPublishRaw();
    channel3220_2.mqttPublishRaw();
    channel3220_3.mqttPublishRaw();
    channel3220_4.mqttPublishRaw();
    channel3220_5.mqttPublishRaw();
    channel3220_6.mqttPublishRaw();
}

void Channel3220::mqttPublishPhysical()
{
    channel3220_0.mqttPublishPhysical();
    channel3220_1.mqttPublishPhysical();
    channel3220_2.mqttPublishPhysical();
    channel3220_3.mqttPublishPhysical();
    channel3220_4.mqttPublishPhysical();
    channel3220_5.mqttPublishPhysical();
    channel3220_6.mqttPublishPhysical();
}
