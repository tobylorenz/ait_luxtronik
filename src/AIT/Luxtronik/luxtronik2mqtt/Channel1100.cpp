/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1100.h"

Channel1100::Channel1100(MqttClient * mqttClient) :
    ChannelBase("1100", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Temperaturen");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Vorlauf");
    m_mqttClient->setTopic("physical/" + m_id + "/2/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Rücklauf");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "Rücklauf-Soll");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "Heissgas");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "Aussentemperatur");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "Brauchwasser-Ist");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/8/$name", "Brauchwasser-Soll");
    m_mqttClient->setTopic("physical/" + m_id + "/8/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/9/$name", "Wärmequelle-Ein");
    m_mqttClient->setTopic("physical/" + m_id + "/9/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/10/$name", "Wärmequelle-Aus");
    m_mqttClient->setTopic("physical/" + m_id + "/10/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/11/$name", "Mischkreis 1-Vorlauf");
    m_mqttClient->setTopic("physical/" + m_id + "/11/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/12/$name", "Mischkreis 1-Vorlauf-Soll");
    m_mqttClient->setTopic("physical/" + m_id + "/12/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/13/$name", "Raumstation");
    m_mqttClient->setTopic("physical/" + m_id + "/13/$unit", "°C");
}

void Channel1100::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", rawToPhysical(getItem(2), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/3", rawToPhysical(getItem(3), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/4", rawToPhysical(getItem(4), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/5", rawToPhysical(getItem(5), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/6", rawToPhysical(getItem(6), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/7", rawToPhysical(getItem(7), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/8", rawToPhysical(getItem(8), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/9", rawToPhysical(getItem(9), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/10", rawToPhysical(getItem(10), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/11", rawToPhysical(getItem(11), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/12", rawToPhysical(getItem(12), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/13", rawToPhysical(getItem(13), 0.1, 1));
}
