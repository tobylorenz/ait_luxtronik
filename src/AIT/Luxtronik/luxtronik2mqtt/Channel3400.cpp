﻿/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel3400.h"

Channel3400::Channel3400(MqttClient * mqttClient) :
    ChannelBase("3400", mqttClient)
{
    mqttClient->setTopic("physical/" + m_id + "/$name", "Heizkurven");

    mqttClient->setTopic("physical/" + m_id + "/2/$name", "Temperatur +/-");
    mqttClient->setTopic("physical/" + m_id + "/2/$range", "-5.0:5.0");
    mqttClient->setTopic("physical/" + m_id + "/2/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/3/$name", "HK Heizkurvenendpunkt");
    mqttClient->setTopic("physical/" + m_id + "/3/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/4/$name", "HK Parallelverschiebung");
    mqttClient->setTopic("physical/" + m_id + "/4/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/5/$name", "HK Absenkung");
    mqttClient->setTopic("physical/" + m_id + "/5/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/5/$name", "HK Festwert Rücklauf");
    mqttClient->setTopic("physical/" + m_id + "/6/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/7/$name", "MK1 Heizkurvenendpunkt");
    mqttClient->setTopic("physical/" + m_id + "/7/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/8/$name", "MK1 Parallelverschiebung");
    mqttClient->setTopic("physical/" + m_id + "/8/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/9/$name", "MK1 Absenkung");
    mqttClient->setTopic("physical/" + m_id + "/9/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/9/$name", "MK1 Festwerk Vorlauf");
    mqttClient->setTopic("physical/" + m_id + "/10/$unit", "°C");
}

void Channel3400::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", rawToPhysical(getItem(2), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/3", rawToPhysical(getItem(3), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/4", rawToPhysical(getItem(4), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/5", rawToPhysical(getItem(5), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/6", rawToPhysical(getItem(6), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/7", rawToPhysical(getItem(7), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/8", rawToPhysical(getItem(8), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/9", rawToPhysical(getItem(9), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/10", rawToPhysical(getItem(10), 0.1, 1));
}
