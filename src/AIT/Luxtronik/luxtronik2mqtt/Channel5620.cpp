/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel5620.h"

/* C++ includes */
#include <iostream>

Channel5620::Channel5620(MqttClient * mqttClient) :
    ChannelBase("5620", mqttClient),
    channel5620_0("0", mqttClient),
    channel5620_1("1", mqttClient),
    channel5620_2("2", mqttClient),
    channel5620_3("3", mqttClient),
    channel5620_4("4", mqttClient),
    channel5620_5("5", mqttClient),
    channel5620_6("6", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Schaltzeiten Tage - Brauchwasser");
}

void Channel5620::mqttPublishRaw()
{
    channel5620_0.mqttPublishRaw();
    channel5620_1.mqttPublishRaw();
    channel5620_2.mqttPublishRaw();
    channel5620_3.mqttPublishRaw();
    channel5620_4.mqttPublishRaw();
    channel5620_5.mqttPublishRaw();
    channel5620_6.mqttPublishRaw();
}

void Channel5620::mqttPublishPhysical()
{
    channel5620_0.mqttPublishPhysical();
    channel5620_1.mqttPublishPhysical();
    channel5620_2.mqttPublishPhysical();
    channel5620_3.mqttPublishPhysical();
    channel5620_4.mqttPublishPhysical();
    channel5620_5.mqttPublishPhysical();
    channel5620_6.mqttPublishPhysical();
}
