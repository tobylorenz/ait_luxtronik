/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1300.h"

Channel1300::Channel1300(MqttClient * mqttClient) :
    ChannelBase("1300", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Ausgänge");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Abtauventil");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "BUP");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "FUP 1");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "HUP");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "Mischer 1 Auf");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "Mischer 1 Zu");
    m_mqttClient->setTopic("physical/" + m_id + "/8/$name", "Ventilation");
    m_mqttClient->setTopic("physical/" + m_id + "/9/$name", "Ventil.-BOSUP");
    m_mqttClient->setTopic("physical/" + m_id + "/10/$name", "Verdichter 1");
    m_mqttClient->setTopic("physical/" + m_id + "/11/$name", "Verdichter 2");
    m_mqttClient->setTopic("physical/" + m_id + "/12/$name", "ZUP - ZIP");
    m_mqttClient->setTopic("physical/" + m_id + "/13/$name", "ZWE 1");
    m_mqttClient->setTopic("physical/" + m_id + "/14/$name", "ZWE 2 - SST");
}

void Channel1300::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", (getItem(2) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/3", (getItem(3) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/4", (getItem(4) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/5", (getItem(5) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/6", (getItem(6) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/7", (getItem(7) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/8", (getItem(8) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/9", (getItem(9) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/10", (getItem(10) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/11", (getItem(11) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/12", (getItem(12) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/13", (getItem(13) == "0") ? "AUS": "EIN");
    m_mqttClient->setTopic("physical/" + m_id + "/14", (getItem(14) == "0") ? "AUS": "EIN");
}
