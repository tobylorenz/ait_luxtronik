/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel2200.h"

Channel2200::Channel2200(MqttClient * mqttClient) :
    ChannelBase("2200", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "System");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "EVU");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Raumstation");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "Einbindung");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "Mischkr1");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "ZWE1 Art");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "ZWE1 Fkt");
    m_mqttClient->setTopic("physical/" + m_id + "/8/$name", "ZWE2 Art");
    m_mqttClient->setTopic("physical/" + m_id + "/9/$name", "ZWE2 Fkt");
    m_mqttClient->setTopic("physical/" + m_id + "/10/$name", "Störung");
    m_mqttClient->setTopic("physical/" + m_id + "/11/$name", "Brauchwasser 1");
    m_mqttClient->setTopic("physical/" + m_id + "/12/$name", "Brauchwasser 2");
    m_mqttClient->setTopic("physical/" + m_id + "/13/$name", "Brauchwasser 3");
    m_mqttClient->setTopic("physical/" + m_id + "/14/$name", "Brauchwasser 4");

    m_mqttClient->setTopic("physical/" + m_id + "/15/$name", "BW+WP max");
    m_mqttClient->setTopic("physical/" + m_id + "/15/$unit", "h");

    m_mqttClient->setTopic("physical/" + m_id + "/16/$name", "Abtzyk max");
    m_mqttClient->setTopic("physical/" + m_id + "/16/$unit", "min");

    m_mqttClient->setTopic("physical/" + m_id + "/17/$name", "Luftabt.");

    m_mqttClient->setTopic("physical/" + m_id + "/18/$name", "L-Abt max");
    m_mqttClient->setTopic("physical/" + m_id + "/18/$range", "5:30");
    m_mqttClient->setTopic("physical/" + m_id + "/18/$unit", "min");

    m_mqttClient->setTopic("physical/" + m_id + "/19/$name", "Abtauen 1");
    m_mqttClient->setTopic("physical/" + m_id + "/20/$name", "Abtauen 2");
    m_mqttClient->setTopic("physical/" + m_id + "/21/$name", "Pumpenopt.");
    m_mqttClient->setTopic("physical/" + m_id + "/22/$name", "Zusatzp.");
    m_mqttClient->setTopic("physical/" + m_id + "/23/$name", "Zugang");
    m_mqttClient->setTopic("physical/" + m_id + "/24/$name", "ASD");
    m_mqttClient->setTopic("physical/" + m_id + "/25/$name", "Ueberw. VD");
    m_mqttClient->setTopic("physical/" + m_id + "/26/$name", "Regelung");
    m_mqttClient->setTopic("physical/" + m_id + "/27/$name", "Ausheizen");
    m_mqttClient->setTopic("physical/" + m_id + "/28/$name", "Parallelbetrieb");
    m_mqttClient->setTopic("physical/" + m_id + "/29/$name", "elektr. Anode");
    m_mqttClient->setTopic("physical/" + m_id + "/30/$name", "Periode 1");
    m_mqttClient->setTopic("physical/" + m_id + "/30/$range", "0.25:2.00");
    m_mqttClient->setTopic("physical/" + m_id + "/31/$name", "Laufzeit 1");
    m_mqttClient->setTopic("physical/" + m_id + "/31/$range", "0.25:2.00");
}

void Channel2200::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", (getItem(2) == "0") ? "ohne ZWE" : "mit ZWE");
    m_mqttClient->setTopic("physical/" + m_id + "/3", (getItem(3) == "0") ? "Nein" : "Ja");
    m_mqttClient->setTopic("physical/" + m_id + "/4", (getItem(4) == "0") ? "Trennsp" : "Rueckl.");

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Entlade" },
            { 2, "Lade" },
            { 3, "Kühl" }
        };
        uint8_t itemValue = std::stoul(getItem(5));
        m_mqttClient->setTopic("physical/" + m_id + "/5", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/5", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Heizstab" },
            { 2, "Therme" },
            { 3, "Kessel" }
        };
        uint8_t itemValue = std::stoul(getItem(6));
        m_mqttClient->setTopic("physical/" + m_id + "/6", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/6", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Hz u. Bw" },
            { 2, "Heizen" }
        };
        uint8_t itemValue = std::stoul(getItem(7));
        m_mqttClient->setTopic("physical/" + m_id + "/7", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/7", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Heizstab" },
            { 2, "Therme" },
            { 3, "Kessel" }
        };
        uint8_t itemValue = std::stoul(getItem(8));
        m_mqttClient->setTopic("physical/" + m_id + "/8", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/8", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Heizen" },
            { 2, "Brauchw." }
        };
        uint8_t itemValue = std::stoul(getItem(9));
        m_mqttClient->setTopic("physical/" + m_id + "/9", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/9", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/10", (getItem(10) == "0") ? "mit ZWE" : "ohne ZWE");
    m_mqttClient->setTopic("physical/" + m_id + "/11", (getItem(11) == "0") ? "Fuehler" : "Therm.");
    m_mqttClient->setTopic("physical/" + m_id + "/12", (getItem(12) == "0") ? "mit 1VD" : "mit 2VD");
    m_mqttClient->setTopic("physical/" + m_id + "/13", (getItem(13) == "0") ? "mit ZUP" : "ohne ZUP");
    m_mqttClient->setTopic("physical/" + m_id + "/14", (getItem(14) == "0") ? "Sollwert" : "Maxwert");

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "0,0" },
            { 5, "0,5" },
            { 10, "1,0" },
            { 15, "1,5" },
            { 20, "2,0" },
            { 25, "2,5" },
            { 30, "3,0" },
            { 35, "3,5" },
            { 40, "4,0" },
            { 45, "4,5" },
            { 50, "5,0" },
            { 55, "5,5" },
            { 60, "6,0" },
            { 65, "6,5" },
            { 70, "7,0" },
            { 75, "7,5" },
            { 80, "8,0"}
        };
        uint8_t itemValue = std::stoul(getItem(15));
        m_mqttClient->setTopic("physical/" + m_id + "/15", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/15", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "45" },
            { 1, "60" },
            { 2, "90" },
            { 3, "120" },
            { 4, "180" },
            { 5, "240" }
        };
        uint8_t itemValue = std::stoul(getItem(16));
        m_mqttClient->setTopic("physical/" + m_id + "/16", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/16", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/17", (getItem(17) == "0") ? "Nein" : "Ja");

    m_mqttClient->setTopic("physical/" + m_id + "/18", std::to_string(5 + std::stoul(getItem(18))));

    m_mqttClient->setTopic("physical/" + m_id + "/19", (getItem(19) == "0") ? "Abt 1" : "Abt 2");
    m_mqttClient->setTopic("physical/" + m_id + "/20", (getItem(20) == "0") ? "mit 1VD" : "mit 2VD");
    m_mqttClient->setTopic("physical/" + m_id + "/21", (getItem(21) == "0") ? "Nein" : "Ja");
    m_mqttClient->setTopic("physical/" + m_id + "/22", (getItem(22) == "0") ? "ZUP" : "ZIP");
    m_mqttClient->setTopic("physical/" + m_id + "/23", (getItem(23) == "0") ? "Inst" : "KD");

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Durchfluss" },
            { 2, "Soledruck" }
        };
        uint8_t itemValue = std::stoul(getItem(24));
        m_mqttClient->setTopic("physical/" + m_id + "/24", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/24", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/25", (getItem(25) == "0") ? "Aus" : "Ein");
    m_mqttClient->setTopic("physical/" + m_id + "/26", (getItem(26) == "0") ? "AT-Abh." : "Festt.");
    m_mqttClient->setTopic("physical/" + m_id + "/27", (getItem(27) == "0") ? "m. Misch" : "o. Misch");
    m_mqttClient->setTopic("physical/" + m_id + "/28", (getItem(28) == "0") ? "Nein" : "Ja");
    m_mqttClient->setTopic("physical/" + m_id + "/29", (getItem(29) == "0") ? "Nein" : "Ja");

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 50, "0,5" },
            { 75, "0,75" },
            { 100, "1,0" },
            { 125, "1,25" },
            { 150, "1,5" },
            { 175, "1,75" },
            { 200, "2,0" }
        };
        uint8_t itemValue = std::stoul(getItem(30));
        m_mqttClient->setTopic("physical/" + m_id + "/30", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/30", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "" },
            { 2, "0,25" },
            { 5, "0,5" },
            { 7, "0,75" },
            { 10, "1,0" },
            { 12, "1,25" },
            { 15, "1,5" },
            { 17, "1,75" },
            { 20, "2,0" }
        };
        uint8_t itemValue = std::stoul(getItem(31));
        m_mqttClient->setTopic("physical/" + m_id + "/31", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/31", "");
    }
}
