/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1400.h"

Channel1400::Channel1400(MqttClient * mqttClient) :
    ChannelBase("1400", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Ablaufzeiten");

    m_mqttClient->setTopic("physical/" + m_id + "/2-4/$name", "WP seit");
    m_mqttClient->setTopic("physical/" + m_id + "/5-7/$name", "ZWE1 seit");
    m_mqttClient->setTopic("physical/" + m_id + "/8-10/$name", "ZWE2 seit");
    m_mqttClient->setTopic("physical/" + m_id + "/11/$name", "Netzeinv.");
    m_mqttClient->setTopic("physical/" + m_id + "/12-13/$name", "SSP-Zeit-Standzeit");
    m_mqttClient->setTopic("physical/" + m_id + "/14-15/$name", "SSP-Zeit-Einschverz");
    m_mqttClient->setTopic("physical/" + m_id + "/16-18/$name", "VD-Stand");
    m_mqttClient->setTopic("physical/" + m_id + "/19-21/$name", "HRM-Zeit");
    m_mqttClient->setTopic("physical/" + m_id + "/22-24/$name", "HRW-Zeit");
    m_mqttClient->setTopic("physical/" + m_id + "/25-27/$name", "TDI-Seit");
    m_mqttClient->setTopic("physical/" + m_id + "/28-30/$name", "Sperre BW");
}

void Channel1400::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2-4", hmsToTime(getItem(2), getItem(3), getItem(4)));
    m_mqttClient->setTopic("physical/" + m_id + "/5-7", hmsToTime(getItem(5), getItem(6), getItem(7)));
    m_mqttClient->setTopic("physical/" + m_id + "/8-10", hmsToTime(getItem(8), getItem(9), getItem(10)));
    m_mqttClient->setTopic("physical/" + m_id + "/11", getItem(11));
    m_mqttClient->setTopic("physical/" + m_id + "/12-13", hmsToTime("0", getItem(12), getItem(13)));
    m_mqttClient->setTopic("physical/" + m_id + "/14-15", hmsToTime("0", getItem(14), getItem(15)));
    m_mqttClient->setTopic("physical/" + m_id + "/16-18", hmsToTime(getItem(16), getItem(17), getItem(18)));
    m_mqttClient->setTopic("physical/" + m_id + "/19-21", hmsToTime(getItem(19), getItem(20), getItem(21)));
    m_mqttClient->setTopic("physical/" + m_id + "/22-24", hmsToTime(getItem(22), getItem(23), getItem(24)));
    m_mqttClient->setTopic("physical/" + m_id + "/25-27", hmsToTime(getItem(25), getItem(26), getItem(27)));
    m_mqttClient->setTopic("physical/" + m_id + "/28-30", hmsToTime(getItem(28), getItem(29), getItem(30)));
}
