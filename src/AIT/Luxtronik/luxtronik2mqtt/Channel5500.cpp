/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel5500.h"

/* C++ includes */
#include <iostream>

Channel5500::Channel5500(MqttClient * mqttClient) :
    ChannelBase("5500", mqttClient),
    channel5100(mqttClient),
    channel5200(mqttClient),
    channel5300(mqttClient),
    channel5400(mqttClient)
{
}

void Channel5500::mqttPublishRaw()
{
    channel5100.mqttPublishRaw();
    channel5200.mqttPublishRaw();
    channel5300.mqttPublishRaw();
    channel5400.mqttPublishRaw();
}

void Channel5500::mqttPublishPhysical()
{
    channel5100.mqttPublishPhysical();
    channel5200.mqttPublishPhysical();
    channel5300.mqttPublishPhysical();
    channel5400.mqttPublishPhysical();
}
