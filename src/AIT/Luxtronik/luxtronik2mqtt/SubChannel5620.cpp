/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "SubChannel5620.h"

#include <array>

SubChannel5620::SubChannel5620(std::string id, MqttClient * mqttClient) :
    ChannelBase(id, mqttClient)
{
    static const std::array<std::string, 7> itemTexts = {
        "Sonntag",
        "Montag",
        "Dienstag",
        "Mittwoch",
        "Donnerstag",
        "Freitag",
        "Samstag"
    };
    uint8_t itemValue = std::stoul(m_id);
    m_mqttClient->setTopic("physical/5620/" + m_id + "/$name", itemTexts.at(itemValue));

    m_mqttClient->setTopic("physical/5620/" + m_id + "/3-4/$name", "Tag Anfang 1");
    m_mqttClient->setTopic("physical/5620/" + m_id + "/5-6/$name", "Tag Ende 1");
    m_mqttClient->setTopic("physical/5620/" + m_id + "/7-8/$name", "Tag Anfang 2");
    m_mqttClient->setTopic("physical/5620/" + m_id + "/9-10/$name", "Tag Ende 2");
    m_mqttClient->setTopic("physical/5620/" + m_id + "/11-12/$name", "Nacht Anfang 1");
    m_mqttClient->setTopic("physical/5620/" + m_id + "/13-14/$name", "Nacht Ende 1");
    m_mqttClient->setTopic("physical/5620/" + m_id + "/15-16/$name", "Nacht Anfang 2");
    m_mqttClient->setTopic("physical/5620/" + m_id + "/17-18/$name", "Nacht Ende 2");
}

void SubChannel5620::mqttPublishRaw()
{
    /* line */
    m_mqttClient->setTopic("raw/5620/" + m_id, m_line);

    /* list elements */
    for (uint8_t i = 3; i < m_list.size(); i++) {
        m_mqttClient->setTopic("raw/5620/" + m_id + "/" + std::to_string(i), m_list[i]);
    }
}

void SubChannel5620::mqttPublishPhysical()
{
    m_mqttClient->setTopic("raw/5620/" + m_id + "/3-4", hmsToTime(getItem(3), getItem(4), "0"));
    m_mqttClient->setTopic("raw/5620/" + m_id + "/5-6", hmsToTime(getItem(5), getItem(6), "0"));
    m_mqttClient->setTopic("raw/5620/" + m_id + "/7-8", hmsToTime(getItem(7), getItem(8), "0"));
    m_mqttClient->setTopic("raw/5620/" + m_id + "/9-10", hmsToTime(getItem(9), getItem(10), "0"));
    m_mqttClient->setTopic("raw/5620/" + m_id + "/11-12", hmsToTime(getItem(11), getItem(12), "0"));
    m_mqttClient->setTopic("raw/5620/" + m_id + "/13-14", hmsToTime(getItem(13), getItem(14), "0"));
    m_mqttClient->setTopic("raw/5620/" + m_id + "/15-16", hmsToTime(getItem(15), getItem(16), "0"));
    m_mqttClient->setTopic("raw/5620/" + m_id + "/17-18", hmsToTime(getItem(17), getItem(18), "0"));
}
