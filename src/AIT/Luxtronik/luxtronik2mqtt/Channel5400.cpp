/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel5400.h"

Channel5400::Channel5400(MqttClient * mqttClient) :
    ChannelBase("5400", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Status");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Betriebsart Lüftung");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Schaltuhrzustand");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "SU Stosslüftung");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "Stosslüftung");
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "???");
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "???");
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/8/$name", "???");
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/9/$name", "???");
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/10/$name", "???");
    // @todo m_mqttClient->setTopic("physical/" + m_id + "/11/$name", "???");
    // @todo Verbleibende Zeit STL: 0=0min
}

void Channel5400::mqttPublishPhysical()
{
    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Automatik" },
            { 1, "-" },
            { 2, "Party" },
            { 3, "Ferien" },
            { 4, "AUS" }
        };
        uint8_t itemValue = std::stoul(getItem(2));
        m_mqttClient->setTopic("physical/" + m_id + "/2", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/2", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "AUS" },
            { 1, "Tage" },
            { 2, "Nacht" }
        };
        uint8_t itemValue = std::stoul(getItem(3));
        m_mqttClient->setTopic("physical/" + m_id + "/3", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/3", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Woche" },
            { 1, "5+2" },
            { 2, "Einzeltage" }
        };
        uint8_t itemValue = std::stoul(getItem(4));
        m_mqttClient->setTopic("physical/" + m_id + "/4", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/4", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/5", (getItem(5) == "0") ? "AUS" : "EIN");

    // @todo 5400/6-11 ???
}
