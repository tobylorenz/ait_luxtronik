/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel6540.h"

/* C++ includes */
#include <iostream>

Channel6540::Channel6540(MqttClient * mqttClient) :
    ChannelBase("6540", mqttClient),
    channel6540_0("0", mqttClient),
    channel6540_1("1", mqttClient),
    channel6540_2("2", mqttClient),
    channel6540_3("3", mqttClient),
    channel6540_4("4", mqttClient),
    channel6540_5("5", mqttClient),
    channel6540_6("6", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Schaltzeiten Tage");
}

void Channel6540::mqttPublishRaw()
{
    channel6540_0.mqttPublishRaw();
    channel6540_1.mqttPublishRaw();
    channel6540_2.mqttPublishRaw();
    channel6540_3.mqttPublishRaw();
    channel6540_4.mqttPublishRaw();
    channel6540_5.mqttPublishRaw();
    channel6540_6.mqttPublishRaw();
}

void Channel6540::mqttPublishPhysical()
{
    channel6540_0.mqttPublishPhysical();
    channel6540_1.mqttPublishPhysical();
    channel6540_2.mqttPublishPhysical();
    channel6540_3.mqttPublishPhysical();
    channel6540_4.mqttPublishPhysical();
    channel6540_5.mqttPublishPhysical();
    channel6540_6.mqttPublishPhysical();
}
