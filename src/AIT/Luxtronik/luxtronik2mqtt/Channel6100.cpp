/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel6100.h"

Channel6100::Channel6100(MqttClient * mqttClient) :
    ChannelBase("6100", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Temperaturen");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Fußbodenkreis 2");
    m_mqttClient->setTopic("physical/" + m_id + "/2/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Solarkollektor");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "Solarspeicher");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "externe Energiequelle");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$unit", "°C");
}

void Channel6100::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", rawToPhysical(getItem(2), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/3", rawToPhysical(getItem(3), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/4", rawToPhysical(getItem(4), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/5", rawToPhysical(getItem(5), 0.1, 1));
}
