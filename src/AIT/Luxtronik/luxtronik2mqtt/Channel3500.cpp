/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel3500.h"

Channel3500::Channel3500(MqttClient * mqttClient) :
    ChannelBase("3500", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Brauchwasser");

    // @todo 3500/2: ???

    mqttClient->setTopic("physical/" + m_id + "/3/$name", "aktuelle Betriebsart");
    mqttClient->setTopic("physical/" + m_id + "/4/$name", "Steuereingang");
    mqttClient->setTopic("physical/" + m_id + "/5/$name", "Schaltuhr BW");

    mqttClient->setTopic("physical/" + m_id + "/6/$name", "Solltemperatur");
    mqttClient->setTopic("physical/" + m_id + "/6/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/7/$name", "Isttemperatur");
    mqttClient->setTopic("physical/" + m_id + "/7/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/8/$name", "Schnellladung");
    mqttClient->setTopic("physical/" + m_id + "/9/$name", "BAN");
    mqttClient->setTopic("physical/" + m_id + "/10/$name", "BUP");
    mqttClient->setTopic("physical/" + m_id + "/11/$name", "Zwangsbrauchwasser");

    // @todo 3500/12: ???
    // @todo 3500/13: ???

    mqttClient->setTopic("physical/" + m_id + "/14/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/15/$unit", "°C");

    // @todo 3500/16 ???

    mqttClient->setTopic("physical/" + m_id + "/17/$unit", "°C");

    mqttClient->setTopic("physical/" + m_id + "/18/$name", "Thermische Desinf.");
}

void Channel3500::mqttPublishPhysical()
{
    // @todo 3500/2: ???

    try {
        static const std::vector<std::string> itemTexts = {
            "Automatik",
            "ZWE",
            "Party",
            "Ferien",
            "Aus"
        };
        uint8_t itemValue = std::stoul(getItem(3));
        m_mqttClient->setTopic("physical/" + m_id + "/3", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/3", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/4", (getItem(4) == "0") ? "Thermostat" : "Fühler");

    m_mqttClient->setTopic("physical/" + m_id + "/5", (getItem(5) == "0") ? "Freigabe" : "Sperre");

    m_mqttClient->setTopic("physical/" + m_id + "/6", rawToPhysical(getItem(6), 0.1, 1));

    m_mqttClient->setTopic("physical/" + m_id + "/7", rawToPhysical(getItem(7), 0.1, 1));

    m_mqttClient->setTopic("physical/" + m_id + "/8", (getItem(8) == "0") ? "Aus" : "Ein");

    m_mqttClient->setTopic("physical/" + m_id + "/9", (getItem(9) == "0") ? "Aus" : "Ein");

    m_mqttClient->setTopic("physical/" + m_id + "/10", (getItem(10) == "0") ? "Aus" : "Ein");

    m_mqttClient->setTopic("physical/" + m_id + "/11", (getItem(11) == "0") ? "Aus" : "Ein");

    // @todo 3500/12: ???

    // @todo 3500/13: ???

    m_mqttClient->setTopic("physical/" + m_id + "/14", rawToPhysical(getItem(14), 0.1, 1));

    m_mqttClient->setTopic("physical/" + m_id + "/15", rawToPhysical(getItem(15), 0.1, 1));

    // @todo 3500/16 ???

    m_mqttClient->setTopic("physical/" + m_id + "/17", rawToPhysical(getItem(17), 0.1, 1));

    m_mqttClient->setTopic("physical/" + m_id + "/18", (getItem(18) == "0") ? "Aus" : "Ein");
}
