/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel2700.h"

#include <array>

Channel2700::Channel2700(MqttClient * mqttClient) :
    ChannelBase("2700", mqttClient)
{
    mqttClient->setTopic("physical/" + m_id + "/$name", "Datum+Uhrzeit");

    m_mqttClient->setTopic("physical/" + m_id + "/2-4/$name", "Datum");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "Wochentag");
    m_mqttClient->setTopic("physical/" + m_id + "/6-8/$name", "Uhrzeit");
}

void Channel2700::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2-4", ymdToDate(getItem(2), getItem(3), getItem(4)));

    try {
        static const std::array<std::string, 7> itemTexts = {
            "Sonntag",
            "Montag",
            "Dienstag",
            "Mittwoch",
            "Donnerstag",
            "Freitag",
            "Samstag"
        };
        uint8_t itemValue = std::stoul(getItem(5));
        m_mqttClient->setTopic("physical/" + m_id + "/5", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/5", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/6-8", hmsToTime(getItem(6), getItem(7), getItem(8)));
}
