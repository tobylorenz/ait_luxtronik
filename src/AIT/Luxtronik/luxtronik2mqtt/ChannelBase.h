/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

/* C++ includes */
#include <cstdint>
#include <string>
#include <vector>

/* project internal includes */
#include "MqttClient.h"

class ChannelBase
{
public:
    ChannelBase(std::string id, MqttClient * mqttClient);
    virtual ~ChannelBase();

    /** set line */
    virtual void setLine(const std::string & line);

    /** publish raw values via MQTT */
    virtual void mqttPublishRaw();

    /** publish physical values via MQTT */
    virtual void mqttPublishPhysical() = 0;

    /** publish MQTT */
    virtual void mqttPublish();

protected:
    /** id of this channel, e.g. 1800 */
    std::string m_id;

    /** reference to MQTT client */
    MqttClient * m_mqttClient;

    /** line */
    std::string m_line;

    /** line split into list items */
    std::vector<std::string> m_list;

    /**
     * Returns the list item at index.
     * If the list is incomplete, return 0.
     *
     * @param index index
     * @return list item
     */
    std::string getItem(uint8_t index) const;

    /**
     * split line into list
     *
     * @param[out] list item list
     * @param[in] line line
     */
    void splitLine(std::vector<std::string> & list, std::string & line);

    /**
     * convert raw value as string to physical value as string
     *
     * @param[in] rawString raw string
     * @param[in] factor conversion factor
     * @param[in] precision physical value string precision
     * @return physical string
     */
    std::string rawToPhysical(const std::string & rawString, double factor, uint8_t precision);

    /**
     * Convert three strings being year, month, and day into a combined date string
     *
     * @param[in] year year string
     * @param[in] month month string
     * @param[in] day day string
     * @return date date string
     */
    std::string ymdToDate(const std::string & year, const std::string & month, const std::string & day);

    /**
     * Convert three strings being hour, minute, and second into a combined time string
     *
     * @param[in] hour hour string
     * @param[in] minute minute string
     * @param[in] second second string
     * @param[in] hourFill Fill up hour field with hourFill zeros
     * @return time time string
     */
    std::string hmsToTime(const std::string hour, const std::string minute, const std::string second, uint8_t hourFill = 2);

    /**
     * convert raw string in seconds to H:M:S
     *
     * @param[in] rawString raw string
     * @return H:M:S
     */
    std::string secondsToHMS(const std::string & rawString);
};
