/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel6410.h"

Channel6410::Channel6410(MqttClient * mqttClient) :
    ChannelBase("6410", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Systemeinstellungen");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Art des ZWE3");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Funktion des ZWE3");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "Einstellung Hz mit/ohne SUP");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "Typ des Mischkreis 2");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "Kühlregelung fest/At-abhängig");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "Schwimmbad mit/ohne ZUP");
    m_mqttClient->setTopic("physical/" + m_id + "/8/$name", "Mindestlaufzeit SWAN");
    m_mqttClient->setTopic("physical/" + m_id + "/9/$name", "Periode 2");
    m_mqttClient->setTopic("physical/" + m_id + "/10/$name", "Laufzeit 2");
}

void Channel6410::mqttPublishPhysical()
{
    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Heizstab" },
            { 2, "Kessel" }
        };
        uint8_t itemValue = std::stoul(getItem(2));
        m_mqttClient->setTopic("physical/" + m_id + "/2", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/2", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Hz + Bw" },
            { 2, "Brauchwasser" }
        };
        uint8_t itemValue = std::stoul(getItem(3));
        m_mqttClient->setTopic("physical/" + m_id + "/3", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/3", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/4", (getItem(4) == "0") ? "ohne SUP" : "mit SUP");

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Nein" },
            { 1, "Entlade" },
            { 2, "Lade" },
            { 3, "Kühl" },
            { 4, "Hz und Kühl" }
        };
        uint8_t itemValue = std::stoul(getItem(5));
        m_mqttClient->setTopic("physical/" + m_id + "/5", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/5", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/6", (getItem(6) == "0") ? "Festtemperatur" : "AT-abhängig");
    m_mqttClient->setTopic("physical/" + m_id + "/7", (getItem(7) == "0") ? "mit SUP" : "ohne SUP");

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "0,0" },
            { 5, "0,5" },
            { 10, "1,0" },
            { 15, "1,5" },
            { 20, "2,0" },
            { 25, "2,5" },
            { 30, "3,0" },
            { 35, "3,5" },
            { 40, "4,0" },
            { 45, "4,5" },
            { 50, "5,0" }
        };
        uint8_t itemValue = std::stoul(getItem(8));
        m_mqttClient->setTopic("physical/" + m_id + "/8", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/8", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "0" },
            { 50, "0,5" },
            { 75, "0,75" },
            { 100, "1,0" },
            { 125, "1,25" },
            { 150, "1,5" },
            { 175, "1,75" },
            { 200, "2,0" }
        };
        uint8_t itemValue = std::stoul(getItem(9));
        m_mqttClient->setTopic("physical/" + m_id + "/9", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/9", "");
    }

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "0" },
            { 50, "0,5" },
            { 75, "0,75" },
            { 100, "1,0" },
            { 125, "1,25" },
            { 150, "1,5" },
            { 175, "1,75" },
            { 200, "2,0" }
        };
        uint8_t itemValue = std::stoul(getItem(10));
        m_mqttClient->setTopic("physical/" + m_id + "/10", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/10", "");
    }
}
