/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

/* project internal includes */
#include "ChannelBase.h"
#include "SubChannel1600.h"

/** Abschaltungen */
class Channel1600 : public ChannelBase {
public:
    Channel1600(MqttClient * mqttClient);

    virtual void mqttPublishRaw() override;
    virtual void mqttPublishPhysical() override;

    SubChannel1600 channel1600_1600;
    SubChannel1600 channel1600_1601;
    SubChannel1600 channel1600_1602;
    SubChannel1600 channel1600_1603;
    SubChannel1600 channel1600_1604;
};
