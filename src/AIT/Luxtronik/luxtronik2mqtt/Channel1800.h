/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

/* project internal includes */
#include "ChannelBase.h"
#include "Channel1100.h"
#include "Channel1200.h"
#include "Channel1300.h"
#include "Channel1400.h"
#include "Channel1450.h"
#include "Channel1500.h"
#include "Channel1600.h"
#include "Channel1700.h"

class Channel1800 : public ChannelBase {
public:
    Channel1800(MqttClient * mqttClient);

    virtual void mqttPublishRaw() override;
    virtual void mqttPublishPhysical() override;

    Channel1100 channel1100;
    Channel1200 channel1200;
    Channel1300 channel1300;
    Channel1400 channel1400;
    Channel1450 channel1450;
    Channel1500 channel1500;
    Channel1600 channel1600;
    Channel1700 channel1700;
};
