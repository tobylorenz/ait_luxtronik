/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

/* project internal includes */
#include "ChannelBase.h"
#include "SubChannel3150.h"

/** Schaltzeiten Tage - Heizkreis */
class Channel3150 : public ChannelBase {
public:
    Channel3150(MqttClient * mqttClient);

    virtual void mqttPublishRaw() override;
    virtual void mqttPublishPhysical() override;

    SubChannel3150 channel3150_0;
    SubChannel3150 channel3150_1;
    SubChannel3150 channel3150_2;
    SubChannel3150 channel3150_3;
    SubChannel3150 channel3150_4;
    SubChannel3150 channel3150_5;
    SubChannel3150 channel3150_6;
};
