/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel6200.h"

Channel6200::Channel6200(MqttClient * mqttClient) :
    ChannelBase("6200", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Eingänge");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Schwimmbadthermostat");
}

void Channel6200::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", (getItem(2) == "0") ? "AUS" : "EIN");
}
