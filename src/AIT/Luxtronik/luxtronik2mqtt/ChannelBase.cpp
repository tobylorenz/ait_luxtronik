/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ChannelBase.h"

/* C++ includes */
#include <iomanip>
#include <iostream>
#include <sstream>

ChannelBase::ChannelBase(std::string id, MqttClient * mqttClient) :
    m_id(id),
    m_mqttClient(mqttClient),
    m_line(),
    m_list()
{
}

ChannelBase::~ChannelBase()
{
}

void ChannelBase::setLine(const std::string & line)
{
    m_line = line;
    splitLine(m_list, m_line);

    mqttPublish();
}

void ChannelBase::mqttPublishRaw()
{
    /* line */
    m_mqttClient->setTopic("raw/" + m_id, m_line);

    /* list elements */
    for (uint8_t i = 2; i < m_list.size(); i++) {
        m_mqttClient->setTopic("raw/" + m_id + "/" + std::to_string(i), m_list[i]);
    }
}

void ChannelBase::mqttPublish()
{
    mqttPublishRaw();
    mqttPublishPhysical();
}

std::string ChannelBase::getItem(uint8_t index) const
{
    return (index < m_list.size()) ? m_list.at(index) : "";
}

void ChannelBase::splitLine(std::vector<std::string> & list, std::string & line)
{
    /* clear the list first */
    list.clear();

    /* split line into items using delimiters ; and , */
    std::istringstream iss1(line);
    std::string item1;
    while(std::getline(iss1, item1, ';')) {
        std::istringstream iss2(item1);
        std::string item2;
        while(std::getline(iss2, item2, ',')) {
            list.push_back(item2);
        }
    }
}

std::string ChannelBase::rawToPhysical(const std::string & rawString, double factor, uint8_t precision)
{
    try {
        double rawValue = std::stod(rawString);
        double physicalValue = rawValue * factor; // + offset
        std::ostringstream oss;
        oss << std::fixed << std::setprecision(precision) << physicalValue;
        return oss.str();
    } catch (const std::exception &) {
        return "";
    }
}

std::string ChannelBase::ymdToDate(const std::string & year, const std::string & month, const std::string & day)
{
    try {
        std::ostringstream oss;
        oss << "20"
            << std::setw(2) << std::setfill('0') << std::stoul(year) << "-"
            << std::setw(2) << std::setfill('0') << std::stoul(month) << "-"
            << std::setw(2) << std::setfill('0') << std::stoul(day);
        return oss.str();
    } catch (const std::exception &) {
        return "";
    }
}

std::string ChannelBase::hmsToTime(const std::string hour, const std::string minute, const std::string second, uint8_t hourFill)
{
    try {
        std::ostringstream oss;
        oss << std::setw(hourFill) << std::setfill('0') << std::stoul(hour) << ":"
            << std::setw(2) << std::setfill('0') << std::stoul(minute) << ":"
            << std::setw(2) << std::setfill('0') << std::stoul(second);
        return oss.str();
    } catch (const std::exception &) {
        return "";
    }
}

std::string ChannelBase::secondsToHMS(const std::string & rawString)
{
    try {
        uint32_t rawValue = std::stoul(rawString);
        uint32_t s = rawValue % 60;
        rawValue = (rawValue - s) / 60;
        uint32_t m = rawValue % 60;
        rawValue = (rawValue - m) / 60;
        uint32_t h = rawValue;
        std::ostringstream oss;
        oss << h << ":"
            << std::setw(2) << std::setfill('0') << m << ":"
            << std::setw(2) << std::setfill('0') << s;
        return oss.str();
    } catch (const std::exception &) {
        return "";
    }
}
