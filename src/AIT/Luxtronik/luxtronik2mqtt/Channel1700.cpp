/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel1700.h"

Channel1700::Channel1700(MqttClient * mqttClient) :
    ChannelBase("1700", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Anlagenstatus");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "WP-Typ");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "SW-Stand");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "Biv.-Stufe");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "Betr.-Zust.");
    m_mqttClient->setTopic("physical/" + m_id + "/6-8/$name", "Einschaltzeitpunkt (Datum)");
    m_mqttClient->setTopic("physical/" + m_id + "/9-11/$name", "Einschaltzeitpunkt (Zeit)");
    m_mqttClient->setTopic("physical/" + m_id + "/12/$name", "Compact");
    m_mqttClient->setTopic("physical/" + m_id + "/13/$name", "Comfort");
}

void Channel1700::mqttPublishPhysical()
{
    try {
        static const std::vector<std::string> itemTexts = {
            "---",
            "SW1", // Sole-Wasser 1 Verdichter
            "SW2", // Sole-Wasser 2 Verdichter
            "WW1", // Wasser-Wasser 1 Verdichter
            "WW2", // Wasser-Wasser 2 Verdichter
            "L1I", // Luft-Wasser 1 Verdichter innen
            "L2I", // Luft-Wasser 2 Verdichter innen
            "L1A", // Luft-Wasser 1 Verdichter außen
            "L2A", // Luft-Wasser 1 Verdichter außen
            "KSW", // Kompaktheizzentrale Sole-Wasser
            "KLW", // Kompaktheizzentrale Luft-Wasser
            "SWC", // Sole-Wasser-Compact
            "LWC", // Luft-Wasser-Compact
            "L2G", // Luft-wasser-Großgerät 2 Verdichter
            "WZS"  // Wärmezentrale Sole-Wasser
        };
        uint8_t itemValue = std::stoul(getItem(2));
        m_mqttClient->setTopic("physical/" + m_id + "/2", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/2", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/3", getItem(3));
    m_mqttClient->setTopic("physical/" + m_id + "/4", getItem(4));

    try {
        static const std::map<uint8_t, std::string> itemTexts = {
            { 0, "Heizen" },
            { 1, "Brauchwasser" },
            { 2, "Schwimmbad" },
            { 3, "EVU-Sperre" },
            { 4, "Abtauen" },
            { 5, "Bereitschaft" }
        };
        uint8_t itemValue = std::stoul(getItem(5));
        m_mqttClient->setTopic("physical/" + m_id + "/5", itemTexts.at(itemValue));
    } catch (const std::exception &) {
        m_mqttClient->setTopic("physical/" + m_id + "/5", "");
    }

    m_mqttClient->setTopic("physical/" + m_id + "/6-8", ymdToDate(getItem(8), getItem(7), getItem(6)));
    m_mqttClient->setTopic("physical/" + m_id + "/9-11", hmsToTime(getItem(9), getItem(10), getItem(11)));
    m_mqttClient->setTopic("physical/" + m_id + "/12", (getItem(12) == "0") ? "nicht vorhanden" : "vorhanden");
    m_mqttClient->setTopic("physical/" + m_id + "/13", (getItem(13) == "0") ? "nicht vorhanden" : "vorhanden");
}
