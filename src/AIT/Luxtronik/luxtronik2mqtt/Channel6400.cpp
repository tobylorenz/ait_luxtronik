/*
 * Copyright (C) 2018-2022 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Channel6400.h"

Channel6400::Channel6400(MqttClient * mqttClient) :
    ChannelBase("6400", mqttClient)
{
    m_mqttClient->setTopic("physical/" + m_id + "/$name", "Temperaturen");

    m_mqttClient->setTopic("physical/" + m_id + "/2/$name", "Temperaturdifferenz Ein");
    m_mqttClient->setTopic("physical/" + m_id + "/2/$unit", "K");

    m_mqttClient->setTopic("physical/" + m_id + "/3/$name", "Temperaturdifferenz Aus");
    m_mqttClient->setTopic("physical/" + m_id + "/3/$unit", "K");

    m_mqttClient->setTopic("physical/" + m_id + "/4/$name", "Temperaturdifferenz Max");
    m_mqttClient->setTopic("physical/" + m_id + "/4/$unit", "°C");

    m_mqttClient->setTopic("physical/" + m_id + "/5/$name", "Temp. ext. Energiequelle Heizen");
    m_mqttClient->setTopic("physical/" + m_id + "/5/$unit", "K");

    m_mqttClient->setTopic("physical/" + m_id + "/6/$name", "Temp. ext. Energiequelle Brauchw.");
    m_mqttClient->setTopic("physical/" + m_id + "/6/$unit", "K");

    m_mqttClient->setTopic("physical/" + m_id + "/7/$name", "Temp. Vorlauf 2. VD bei Brauchw.");
    m_mqttClient->setTopic("physical/" + m_id + "/7/$unit", "°C");
}

void Channel6400::mqttPublishPhysical()
{
    m_mqttClient->setTopic("physical/" + m_id + "/2", rawToPhysical(getItem(2), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/3", rawToPhysical(getItem(3), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/4", rawToPhysical(getItem(4), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/5", rawToPhysical(getItem(5), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/6", rawToPhysical(getItem(6), 0.1, 1));
    m_mqttClient->setTopic("physical/" + m_id + "/7", rawToPhysical(getItem(7), 0.1, 1));
}
