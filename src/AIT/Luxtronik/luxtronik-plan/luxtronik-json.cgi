#!/usr/bin/python

import json
import os
import subprocess
import sys

# define topics
topics = {
    'heizung/luxtronik/physical/1100/2': '', # Vorlauf
    'heizung/luxtronik/physical/1100/3': '', # Ruecklauf
    'heizung/luxtronik/physical/1100/4': '', # Ruecklauf-Soll
    'heizung/luxtronik/physical/1100/5': '', # Heissgas
    'heizung/luxtronik/physical/1100/6': '', # Aussentemperatur
    'heizung/luxtronik/physical/1100/7': '', # Brauchwasser-Ist
    'heizung/luxtronik/physical/1100/8': '', # Brauchwasser-Soll
    'heizung/luxtronik/physical/1100/9': '', # Waermequelle-Ein
    'heizung/luxtronik/physical/1100/10': '', # Waermequelle-Aus
    'heizung/luxtronik/physical/1100/11': '', # Mischkreis 1-Vorlauf
    'heizung/luxtronik/physical/1100/12': '', # Mischkreis 1-Vorlauf-Soll
    'heizung/luxtronik/physical/1100/13': '', # Raumstation
    'heizung/luxtronik/physical/1200/2': '', # ASD
    'heizung/luxtronik/physical/1200/3': '', # EVU
    'heizung/luxtronik/physical/1200/4': '', # HD
    'heizung/luxtronik/physical/1200/5': '', # MOT
    'heizung/luxtronik/physical/1200/6': '', # ND
    'heizung/luxtronik/physical/1200/7': '', # PEX
    'heizung/luxtronik/physical/1300/2': '', # Abtauventil
    'heizung/luxtronik/physical/1300/3': '', # BUP
    'heizung/luxtronik/physical/1300/4': '', # FUP 1
    'heizung/luxtronik/physical/1300/5': '', # HUP
    'heizung/luxtronik/physical/1300/6': '', # Mischer 1 Auf
    'heizung/luxtronik/physical/1300/7': '', # Mischer 1 Zu
    'heizung/luxtronik/physical/1300/8': '', # Ventilation
    'heizung/luxtronik/physical/1300/9': '', # Ventil.-BOSUP
    'heizung/luxtronik/physical/1300/10': '', # Verdichter 1
    'heizung/luxtronik/physical/1300/11': '', # Verdichter 2
    'heizung/luxtronik/physical/1300/12': '', # ZUP - ZIP
    'heizung/luxtronik/physical/1300/13': '', # ZWE 1
    'heizung/luxtronik/physical/1300/14': '' # ZWE 2 - SST
}

# get topic values by calling mosquitto_sub
args = []
args.append('mosquitto_sub')
args.append('-h localhost')
count = 0
for topic_name in topics:
    args.append('-t %s' % (topic_name))
    count = count + 1
args.append('-C %s' % (count))
args.append('-v')
process = subprocess.Popen(' '.join(args), stdout=subprocess.PIPE, shell=True)
while True:
    line = process.stdout.readline().rstrip()
    if line != '':
        (topic_name, topic_value) = line.split(' ')
        topics[topic_name] = topic_value
    else:
        break

# print as json
body = json.dumps(topics, sort_keys=True, indent=4)
print('Status: 200 OK')
print('Content-Type: application/json')
print('Content-Length: %d' % (len(body)))
print('')
print(body)
